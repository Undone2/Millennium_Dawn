add_namespace = georg
add_namespace = georg_chech
add_namespace = georg_militar
add_namespace = georg_test
add_namespace = georg_military
add_namespace = georg_revol
add_namespace = georg_war2008
add_namespace = georgia_news
add_namespace = geo_misc

country_event = {
	id = georg.1000
	title = georg.1000.t
	desc = georg.1000.d
	picture = GFX_era_of_shervad

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.1000.a
		log = "[GetDateText]: [This.GetName]: georg.1000.a executed"
	}
}

country_event = {
	id = georg.234
	title = georg.234.t
	desc = georg.234.d
	picture = GFX_geopolice_game_event

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.234.a
		log = "[GetDateText]: [This.GetName]: georg.234.a executed"
	}
}

country_event = {
	id = georg.1244
	title = georg.1244.t
	desc = georg.1244.d
	picture = GFX_georg_army2008

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.1244.a
		log = "[GetDateText]: [This.GetName]: georg.1244.a executed"
		add_equipment_to_stockpile = {
			type = CV_MR_Fighter2 	#su 30
			amount = 4
			producer = SOV
		}
		add_equipment_to_stockpile = {
			type = transport_plane3
			amount = 1
			producer = SOV
		}
	}
}
country_event = {
	id = georg.1243
	title = georg.1243.t
	desc = georg.1243.d
	picture = GFX_war_osset

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.1243.a
		log = "[GetDateText]: [This.GetName]: georg.1243.a executed"
		SOV = {
			diplomatic_relation = {
				country = GEO
				relation = military_access
				active = no
			}
		}
	}
}


country_event = {
	id = georg_military.1
	title = georg_military.1.t
	desc = georg_military.1.d
	picture = GFX_british_military

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_military.1.a
		log = "[GetDateText]: [This.GetName]: georg_military.1.a executed"
	}
}

country_event = {
	id = georg_militar.1
	title = georg_militar.1.t
	desc = georg_militar.1.d
	picture = GFX_british_military

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_militar.1.a
		log = "[GetDateText]: [This.GetName]: georg_militar.1 executed"
	}
}

country_event = {
	id = georg_militar.2
	title = georg_militar.2.t
	desc = georg_militar.2.d
	picture = GFX_british_military
	fire_only_once = yes
	is_triggered_only = yes
	option = {
		name = georg_militar.2.a
		log = "[GetDateText]: [This.GetName]: georg_militar.2 executed"
	}
}

country_event = {
	id = georg_chech.1
	title = georg_chech.1.t
	desc = georg_chech.1.d
	picture = GFX_gelayev_abkhaz

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_chech.1.a
		log = "[GetDateText]: [This.GetName]: georg_chech.1 executed"
		add_war_support = 0.05
		add_political_power = 25
	}
}

country_event = {
	id = georg_chech.2
	title = georg_chech.2.t
	desc = georg_chech.2.d
	picture = GFX_shervad_putin

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_chech.2.a
		log = "[GetDateText]: [This.GetName]: georg_chech.2.a executed"
	}

	option = {
		name = georg_chech.2.b
		trigger = { tag = SOV }
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = GEO value > 30 }
				add = 10
			}
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.2.b executed"
		GEO = { country_event = georg_chech.3 }
	}

	option = {
		name = georg_chech.2.c
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.2.c executed"
	}

}

country_event = {
	id = georg_chech.3
	title = georg_chech.3.t
	desc = georg_chech.3.d
	picture = GFX_georg_specops

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_chech.3.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.3.a executed"
		add_manpower = 1000
		add_political_power = 100
	}
}

country_event = {
	id = georg_chech.4
	title = georg_chech.4.t
	desc = georg_chech.4.d
	picture = GFX_russia_specops

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_chech.4.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.4.a executed"
	}

	option = {
		name = georg_chech.4.b
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.4.b executed"
		add_opinion_modifier = {
			modifier = drama
			target = GEO
		}
		reverse_add_opinion_modifier = {
			modifier = drama
			target = GEO
		}
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = GEO value > 30 }
				add = 10
			}
		}
	}

	option = {
		name = georg_chech.4.c
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.4.c executed"
	}

}

country_event = {
	id = georg_chech.5
	title = georg_chech.5.t
	desc = georg_chech.5.d
	picture = GFX_russia_specops

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_chech.5.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.5.a executed"
	}

	option = {
		name = georg_chech.5.b
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.5.b executed"
		add_opinion_modifier = {
			modifier = declaration_of_friendship
			target = GEO
		}
		reverse_add_opinion_modifier = {
			modifier = declaration_of_friendship
			target = GEO
		}
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = GEO value > 30 }
				add = 10
			}
		}
	}

	option = {
		name = georg_chech.5.c
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.5.c executed"
	}
}

country_event = {
	id = georg_chech.6
	title = georg_chech.6.t
	desc = georg_chech.6.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_chech.6.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.6.a executed"
	}

	option = {
		name = georg_chech.6.b
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.6.b executed"
		ABK = {
			diplomatic_relation = {
				country = SOV
				relation = military_access
				active = no
			}
		}
		SOV = {
			add_political_power = 100
		}
		GEO = {
			add_stability = 0.05
		}
		ai_chance = {
			base = 50
			modifier = {
				has_opinion = { target = GEO value > 30 }
				add = 10
			}
		}
	}

	option = {
		name = georg_chech.6.c
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_chech.6.c executed"
	}
}

country_event = {
	id = georg.3
	title = georg.3.t
	desc = georg.3.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.3.a
		log = "[GetDateText]: [This.GetName]: georg.3.a executed"
		diplomatic_relation = { country = GEO relation = non_aggression_pact active = yes }
		add_days_mission_timeout = {
			mission = ARM_GEO_rebellion
			days = 30
		}
		GEO = { country_event = georg.4 }
	}
	option = {
		name = georg.3.b
		log = "[GetDateText]: [This.GetName]: georg.3.b executed"
		add_political_power = -500
		add_days_mission_timeout = {
			mission = ARM_GEO_rebellion
			days = -30
		}
		add_opinion_modifier = {
			modifier = drama
			target = GEO
		}
		reverse_add_opinion_modifier = {
			modifier = drama
			target = GEO
		}
		GEO = { country_event = georg.5 }
	}
}

country_event = {
	id = georg.4
	title = georg.4.t
	desc = georg.4.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.4.a
		log = "[GetDateText]: [This.GetName]: georg.4.a executed"
	}
}

country_event = {
	id = georg.5
	title = georg.5.t
	desc = georg.5.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg.5.a
		log = "[GetDateText]: [This.GetName]: georg.5.a executed"
	}
}

country_event = {
	id = georg_revol.1
	title = georg_revol.1.t
	desc = georg_revol.1.d
	picture = GFX_rose_revolut

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.1.a
		log = "[GetDateText]: [This.GetName]: georg_revol.1.a executed"
		hidden_effect = {
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 13 }
			add_to_array = { gov_coalition_array = 0 }
			set_temp_variable = { current_cons_popularity = party_pop_array^0 }
			multiply_temp_variable = { current_cons_popularity = 9.5 }
			# Remove most of support, retrack it to emerg communist party
			subtract_from_variable = { party_pop_array^7 = current_cons_popularity }
			add_to_variable = { party_pop_array^0 = current_cons_popularity }
			recalculate_party = yes
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			set_ruling_leader = yes
			set_leader = yes
		}
		add_to_variable = { party_pop_array^0 = 0.30 }
		recalculate_party = yes
		create_country_leader = {
			name = "Nino Burjanadze - Interim Government"
			desc = "lolkek"
			picture = "nino_burjanadze.dds"
			expire = "2004.1.1"
			ideology = Neutral_Autocracy
			traits = {
				political_dancer
			}
		}
	}
}

country_event = {
	id = georg_revol.2
	title = georg_revol.2.t
	desc = georg_revol.2.d
	picture = GFX_anti_bill_protests

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.2.a
		log = "[GetDateText]: [This.GetName]: georg_revol.2.a executed"
	}
}

country_event = {
	id = georg_revol.3
	title = georg_revol.3.t
	desc = georg_revol.3.d
	picture = GFX_era_of_saakash

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.3.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_revol.3.a executed"
	}
}

country_event = {
	id = georg_revol.4
	title = georg_revol.4.t
	desc = georg_revol.4.d
	picture = GFX_kviciani_rebel

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.4.a
		log = "[GetDateText]: [This.GetName]: georg_revol.4.a executed"
	}
}

country_event = {
	id = georg_revol.5
	title = georg_revol.5.t
	desc = georg_revol.5.d
	picture = GFX_kviciani_rebel

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.5.a
		log = "[GetDateText]: [This.GetName]: georg_revol.5.a executed"
	}
}


country_event = {
	id = georg_revol.6
	title = georg_revol.6.t
	desc = georg_revol.6.d
	picture = GFX_monadire

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.6.a
		set_country_flag = GEO_kvic_acc
		log = "[GetDateText]: [This.GetName]: georg_revol.6.a executed"
	}
	option = {
		name = georg_revol.6.b
		set_country_flag = GEO_kvic_den
		log = "[GetDateText]: [This.GetName]: georg_revol.6.b executed"
	}
}

country_event = {
	id = georg_revol.7
	title = georg_revol.7.t
	desc = georg_revol.7.d
	picture = GFX_kviciani_rebel

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.7.a
		log = "[GetDateText]: [This.GetName]: georg_revol.7.a executed"
	}
}

country_event = {
	id = georg_revol.8
	title = georg_revol.8.t
	desc = georg_revol.8.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_revol.8.a
		log = "[GetDateText]: [This.GetName]: georg_revol.8.a executed"
		add_opinion_modifier = {
			modifier = drama
			target = GEO
		}
		reverse_add_opinion_modifier = {
			modifier = drama
			target = GEO
		}
	}
}

country_event = {
	id = georg_war2008.1
	title = georg_war2008.1.t
	desc = georg_war2008.1.d
	picture = GFX_georg_army2008

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.1.a
		log = "[GetDateText]: [This.GetName]: georg_war2008.1.a executed"
		set_country_flag = GEO_flag_chose_atak
		reverse_add_opinion_modifier = { target = SOV modifier = small_decrease }
		add_opinion_modifier = { target = SOV modifier = small_decrease }
		ai_chance = {
			base = 80
			modifier = {
				has_global_flag = GEO_ATTACK_RUS_PATH
				add = 20
			}
		}
	}

	option = {
		name = georg_war2008.1.b
		set_country_flag = GEO_flag_chose_peace
		log = "[GetDateText]: [This.GetName]: georg_war2008.1.b executed"
		ai_chance = {
			base = 20
			modifier = {
				has_global_flag = GEO_WEAKBOY_PATH
				add = 80
			}
		}
	}

	option = {
		name = georg_war2008.1.c
		country_event = georg_war2008.2
		log = "[GetDateText]: [This.GetName]: georg_war2008.1.c executed"
	}
}

country_event = {
	id = georg_war2008.2
	title = georg_war2008.2.t
	desc = georg_war2008.2.d
	picture = GFX_eastern_europe

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.2.a
		country_event = georg_war2008.1
		log = "[GetDateText]: [This.GetName]: georg_war2008.1.a executed"
	}
}

country_event = {
	id = georg_war2008.3
	title = georg_war2008.3.t
	desc = georg_war2008.3.d
	picture = GFX_una_unso_geo_two

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.3.a
		trigger = {
			tag = UKR
		}
		add_stability = 0.05
		add_popularity = {
			ideology = nationalist
			popularity = 0.05
		}
		recalculate_party = yes
		log = "[GetDateText]: [This.GetName]: georg_war2008.3.a executed"
	}

	option = {
		name = georg_war2008.3.b
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.3.b executed"
	}

}

country_event = {
	id = georg_war2008.4
	title = georg_war2008.4.t
	desc = georg_war2008.4.d
	picture = GFX_chechen_volunteers

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.4.a
		trigger = {
			tag = SOV
		}
		add_stability = -0.05
		log = "[GetDateText]: [This.GetName]: georg_war2008.4.a executed"
	}

	option = {
		name = georg_war2008.4.b
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.4.b executed"
	}
}

country_event = {
	id = georg_war2008.5
	title = georg_war2008.5.t
	desc = georg_war2008.5.d
	picture = GFX_medvedev_decision

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.5.a
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.5.a executed"
		GEO = {
			remove_state_core = 706
			remove_state_core = 705
			set_rule = {
				can_join_factions = no
			}
			add_opinion_modifier = {
				modifier = declaration_of_friendship
				target = SOV
			}
			reverse_add_opinion_modifier = {
				modifier = declaration_of_friendship
				target = SOV
			}
		}
	}
}

country_event = {
	id = georg_war2008.6
	title = georg_war2008.6.t
	desc = georg_war2008.6.d
	picture = GFX_medvedev_decision

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.6.a
		ai_chance = {
			factor = 75
			modifier = {
				factor = 0.8
				is_historical_focus_on = yes
				tag = SOV
			}
		}
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.6.a executed"
		GEO = {
			cancel_border_war = {
				dont_fire_events = yes
				defender = 705
				attacker = 707
			}
		}
		GEO = {
			declare_war_on = {
				target = SOV
				type = take_claimed_state
			}
			hidden_effect = {
				SOV = {
					country_event = { id = georg_war2008.10 hours = 120 random_hours = 2 }
				}
			}
		}
	}
	option = {
		name = georg_war2008.6.b
		trigger = {
			tag = SOV
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.6.b executed"
	}
}

country_event = {
	id = georg_war2008.7
	title = georg_war2008.7.t
	desc = georg_war2008.7.d
	picture = GFX_war_osset

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.7.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.7.a executed"
		GEO = {
			annex_country = {
				target = SOO
				transfer_troops = no
			}
			set_country_flag = GEO_won2008_war
		}
	}
}

country_event = {
	id = georg_war2008.8
	title = georg_war2008.8.t
	desc = georg_war2008.8.d
	picture = GFX_vostok_batallion

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.8.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.8.a executed"
		GEO = {
			white_peace = SOO
			set_country_flag = GEO_lost2008_war
		}
	}
}

country_event = {
	id = georg_war2008.9
	title = georg_war2008.9.t
	desc = georg_war2008.9.d
	picture = GFX_vostok_batallion

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = georg_war2008.9.a
		trigger = {
			tag = GEO
		}
		log = "[GetDateText]: [This.GetName]: georg_war2008.9.a executed"
		GEO = {
			white_peace = SOO
			set_country_flag = GEO_lost2008_war
		}
	}
}

country_event = {
	id = georg_war2008.10
	title = georg_war2008.10.t
	desc = georg_war2008.10.d
	picture = GFX_vostok_batallion

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		ai_chance = {
			factor = 75
			modifier = {
				factor = 0.8
				is_historical_focus_on = yes
				tag = SOV
			}
		}
		name = georg_war2008.10.a
		log = "[GetDateText]: [This.GetName]: georg_war2008.10.a executed"
		GEO = {
			white_peace = SOV
			set_country_flag = GEO_lost2008_war
		}
	}

	option = {
		name = georg_war2008.10.b
		log = "[GetDateText]: [This.GetName]: georg_war2008.10.b executed"
	}
}

news_event = {
	id = georgia_news.1
	title = georgia_news.1.t
	desc = georgia_news.1.d
	picture = GFX_arm_tur_prot_ev_news

	is_triggered_only = yes
	major = yes

	option = {
		name = georgia_news.1.o1
		log = "[GetDateText]: [This.GetName]: georgia_news.1.o1 executed"
	}
}

country_event = {
	id = georg_war2008.11
	title = georg_war2008.11.t
	desc = georg_war2008.11.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_paris_convention
	option = {
		name = georg_war2008.11.a
		log = "[GetDateText]: [This.GetName]: georg_war2008.11.a executed"
		ai_chance = { factor = 1 }
		USA = {
			give_guarantee = GEO
		}
	}
	option = {
		name = georg_war2008.11.b
		ai_chance = { factor = 1 }
		log = "[GetDateText]: [This.GetName]: georg_war2008.11.b executed"
	}
}

country_event = {
	id = georg.21
	title = georg.21.t
	desc = georg.21.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_paris_convention
	option = {
		name = georg.21.a
		ai_chance = { factor = 1 }
		set_temp_variable = { int_investment_change = 10 }
		modify_international_investment_effect = yes
		log = "[GetDateText]: [This.GetName]: georg.21.a executed"
		GEO = {
			set_temp_variable = { percent_change = 1 }
			change_domestic_influence_percentage = yes
			random_owned_state = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
	option = {
		name = georg.21.b
		ai_chance = { factor = 1 }
		log = "[GetDateText]: [This.GetName]: georg.21.b executed"
	}
}

country_event = {
	id = georg.22
	title = georg.22.t
	desc = georg.22.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_saakash_ukr
	option = {
		name = georg.22.a
		log = "[GetDateText]: [This.GetName]: georg.22.a executed"
		ai_chance = { factor = 1 }
		GEO = {
			set_country_flag = GEO_saakash_freed
		}
	}
	option = {
		name = georg.22.b
		log = "[GetDateText]: [This.GetName]: georg.22.b executed"
		ai_chance = {
			factor = 75
			modifier = {
				factor = 0.8
				is_historical_focus_on = yes
				tag = GEO
			}
		}
		country_event = georg.23
	}
}

country_event = {
	id = georg.23
	title = georg.23.t
	desc = georg.23.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_saakash_ukr_rel
	option = {
		name = georg.23.a
		log = "[GetDateText]: [This.GetName]: georg.23.a executed"
	}
}

country_event = {
	id = georg.24
	title = georg.24.t
	desc = georg.24.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_bosnia_gov_building_2
	option = {
		name = georg.24.a
		ai_chance = { factor = 1 }
		log = "[GetDateText]: [This.GetName]: georg.24.a executed"
		if = {
			limit = { NOT = { is_in_array = { ruling_party = 23 } } }
			set_temp_variable = { rul_party_temp = 23 }
			change_ruling_party_effect = yes
			set_politics = {
				ruling_party = nationalist
				elections_allowed = no
			}
		}
	}
	option = {
		name = georg.24.b
		ai_chance = { factor = 1 }
		log = "[GetDateText]: [This.GetName]: georg.24.b executed"
		create_country_leader = {
			name = "Djaba The Great"
			picture = "Djaba_yoseliani_schiz.dds"
			ideology = Nat_Autocracy
			traits = { nationalist_Monarchist }
			expire = "2099.1.1"
		}
	}
}

country_event = {
	id = georg.25
	title = georg.25.t
	desc = georg.25.d
	is_triggered_only = yes
	fire_only_once = yes
	picture = GFX_paris_convention
	option = {
		name = georg.25.a
		ai_chance = { factor = 1 }
		GEO = {
			add_to_faction = ROOT
		}
		log = "[GetDateText]: [This.GetName]: georg.25.a executed"
	}
	option = {
		name = georg.25.b
		ai_chance = { factor = 1 }
		hidden_effect = {
			GEO = {
				create_wargoal = {
					type = puppet_wargoal_focus
					target = ROOT
				}
			}
			log = "[GetDateText]: [This.GetName]: georg.25.b executed"
		}
	}
}

#faction invite
country_event = {
	id = georg.26
	title = georg.26.t
	desc = georg.26.d
	picture = GFX_political_deal

	is_triggered_only = yes

	option = {	#Accept
		name = georg.26.a
		log = "[GetDateText]: [This.GetName]: georg.26.a executed"
		SOV = { give_guarantee = GEO }
		GEO = { country_event = diplomatic_response.1 }
		ai_chance = { factor = 3 }
	}

	option = {	#Decline
		name = georg.26.b
		log = "[GetDateText]: [This.GetName]: georg.26.b executed"
		GEO = { country_event = diplomatic_response.2 }
		ai_chance = { factor = 1 }
	}
}

country_event = {
	id = geo_misc.2
	title = geo_misc.2.t
	desc = geo_misc.2.d
	picture = GFX_georg_arm_aze_train

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.2.a
		army_experience = 10
		log = "[GetDateText]: [This.GetName]: geo_misc.2.a executed"
	}
}

country_event = {
	id = geo_misc.3
	title = geo_misc.3.t
	desc = geo_misc.3.d
	picture = GFX_medvedev_decision

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.3.a
		log = "[GetDateText]: [This.GetName]: geo_misc.3.a executed"
		diplomatic_relation = {
			country = GEO
			relation = non_aggression_pact
			active = yes
		}
		ai_chance = { factor = 3 }
	}

	option = {
		name = geo_misc.3.b
		ai_chance = { factor = 1 }
		log = "[GetDateText]: [This.GetName]: geo_misc.3.b executed"
		GEO = { country_event = geo_misc.4 }
	}
}

country_event = {
	id = geo_misc.4
	title = geo_misc.4.t
	desc = geo_misc.4.d
	picture = GFX_vladimir_putin

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.4.a
		log = "[GetDateText]: [This.GetName]: geo_misc.4.a executed"
	}
}
country_event = {
	id = geo_misc.5
	title = geo_misc.5.t
	desc = geo_misc.5.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.5.a
		trigger = {
			tag = SOO
			tag = ABK
		}
		diplomatic_relation = {
			country = GEO
			relation = non_aggression_pact
			active = yes
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.5.a executed"
		ai_chance = { factor = 3 }
	}

	option = {
		name = geo_misc.5.b
		trigger = { tag = SOO }
		ai_chance = { factor = 1 }
		GEO = {
			country_event = geo_misc.6
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.5.b executed"
	}

	option = {
		name = geo_misc.5.c
		trigger = { tag = ABK }
		ai_chance = { factor = 1 }
		GEO = {
			country_event = geo_misc.7
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.5.c executed"
	}

}

country_event = {
	id = geo_misc.6
	title = geo_misc.6.t
	desc = geo_misc.6.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.6.a
		log = "[GetDateText]: [This.GetName]: geo_misc.6.a executed"
	}

}

country_event = {
	id = geo_misc.7
	title = geo_misc.7.t
	desc = geo_misc.7.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.7.a
		log = "[GetDateText]: [This.GetName]: geo_misc.7.a executed"
	}
}

country_event = {
	id = geo_misc.8
	title = geo_misc.8.t
	desc = geo_misc.8.d
	picture = GFX_southern_options

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = geo_misc.8.a
		GEO = {
			puppet = ARM
		}
		ai_chance = {
			factor = 1
			modifier = {
				ARM = { has_communist_government = yes }
				add = 10
			}
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.8.a executed"
	}
	option = {
		name = geo_misc.8.b
		add_opinion_modifier = {
			modifier = hostile_status
			target = ARM
		}
		reverse_add_opinion_modifier = {
			modifier = hostile_status
			target = ARM
		}
		hidden_effect = {
			GEO = {
				create_wargoal = {
					type = annex_everything
					target = ARM
				}
			}
		}
		ai_chance = { factor = 5 }
		log = "[GetDateText]: [This.GetName]: geo_misc.8.b executed"
	}

}

country_event = {
	id = geo_misc.9
	title = geo_misc.9.t
	desc = geo_misc.9.d
	picture = GFX_southern_options

	is_triggered_only = yes

	option = {
		name = geo_misc.9.a
		trigger = {
			tag = ARM
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.9.a executed"
		ai_chance = { factor = 3 }
		GEO = {
			country_event = geo_misc.10
		}
	}

	option = {
		name = geo_misc.9.b
		trigger = {
			tag = ARM
		}
		ai_chance = { factor = 3 }
		add_days_mission_timeout = {
			mission = ARM_GEO_rebellion
			days = -10
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.9.b executed"
	}

	option = {
		name = geo_misc.9.c
		trigger = {
			tag = GEO
		}
		ai_chance = { factor = 3 }
		ARM = {
			country_event = geo_misc.10
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.9.c executed"
	}

	option = {
		name = geo_misc.9.d1
		trigger = {
			tag = GEO
		}
		ai_chance = { factor = 3 }
		add_days_mission_timeout = {
			mission = ARM_GEO_rebellion
			days = -10
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.9.d1 executed"
	}

}

country_event = {
	id = geo_misc.10
	title = geo_misc.10.t
	desc = geo_misc.10.d
	picture = GFX_southern_options

	is_triggered_only = yes

	option = {
		name = geo_misc.10.a
		trigger = {
			tag = ARM
		}
		log = "[GetDateText]: [This.GetName]: geo_misc.10.a executed"
		ai_chance = { factor = 15 }
		remove_mission = ARM_GEO_rebellion
		1033 = {
			remove_core_of = ARM
		}
		set_temp_variable = { tag_index = ARM }
		set_temp_variable = { influence_target = GEO }
		change_influence_percentage = yes
		ARM = {
			add_political_power = 150
			set_country_flag = ARM_GEO_stop_thing
		}
		GEO = {
			add_political_power = 150
			set_country_flag = ARM_GEO_stop_thing
		}
	}

	option = {
		name = geo_misc.10.b
		trigger = { tag = GEO }
		log = "[GetDateText]: [This.GetName]: geo_misc.10.b executed"
		ai_chance = { factor = 3 }
		remove_mission = ARM_GEO_rebellion
		1033 = {
			remove_core_of = ARM
		}
		set_temp_variable = { percent_change = 15 }
		set_temp_variable = { tag_index = ARM }
		set_temp_variable = { influence_target = GEO }
		change_influence_percentage = yes
		ARM = {
			add_political_power = 150
			set_country_flag = ARM_GEO_stop_thing
		}
		GEO = {
			add_political_power = 150
			set_country_flag = ARM_GEO_stop_thing
		}
	}
}

country_event = {
	id = geo_misc.11
	title = geo_misc.11.t
	desc = geo_misc.11.d
	picture = GFX_southern_options

	is_triggered_only = yes

	option = {
		name = geo_misc.11.a
		log = "[GetDateText]: [This.GetName]: geo_misc.11.a executed"
		ai_chance = { factor = 15 }
		add_opinion_modifier = {
			modifier = hostile_status
			target = GEO
		}
		reverse_add_opinion_modifier = {
			modifier = hostile_status
			target = GEO
		}
	}

	option = {
		name = geo_misc.11.b
		log = "[GetDateText]: [This.GetName]: geo_misc.11.a executed"
		ai_chance = { factor = 3 }
		GEO = { puppet = ARM }
	}
}
