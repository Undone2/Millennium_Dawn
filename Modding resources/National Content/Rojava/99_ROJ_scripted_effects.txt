generic_lower_40_percent_influence_of_SYR = {
	set_temp_variable = { influence_gain = influence_total }
	multiply_temp_variable = { influence_gain = 0.4 }
	#Flip to Reduce - vice versa
	multiply_temp_variable = { influence_gain = -1 }
	round_temp_variable = influence_gain
	for_loop_effect = {
		end = influence_array^num
		value = v
		if = {
			limit = {
				check_variable = { influence_array^v = SYR }
			}
			add_to_variable = { influence_array_val^v = influence_gain }
			set_country_flag = found
		}
	}
	if = {
		limit = { NOT = { has_country_flag = found } }
		add_to_array = { influence_array = SYR.id }
		add_to_array = { influence_array_val = influence_gain }
	}
	clr_country_flag = found
	recalculate_influence = yes

	custom_effect_tooltip = decrease_SYR_influence_40_percent_TT
}