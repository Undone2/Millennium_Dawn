# ASEAN Shared Focus Tree
# Author(s): BirdyBoi
# Last Modified By: 02/19/2022
shared_focus = {
	id = ASEAN_asean
	icon = align_to_asean

	cost = 5 # 35 Days
	x = 38
	y = 0
	offset = {
		x = -7
		y = 0
		trigger = {
			NOT = { has_idea = ASEAN_Member }
		}
	}

	# Focus Tree ASEAN Nations
	# Burma
	offset = {
		x = 10
		y = 0
		trigger = { tag = BRM }
	}
	# Singapore
	offset = {
		x = 20
		y = 0
		trigger = { tag = SIN }
	}
	# Philippines
	offset = {
		x = 32
		y = 0
		trigger = { tag = PHI }
	}
	# Thailand
	offset = {
		x = 50
		y = 0
		trigger = { tag = SIA }
	}

	# All ASEAN members and possible nations
	allow_branch = {
		OR = {
			tag = FIJ #Fiji
			tag = PLY #Polynesia
			tag = SRI #Sri Lanka
			tag = AST #Australia
			tag = NZL #New Zealand
			tag = PAP #Papua New Guinea
			tag = BAN #Bangladesh
			tag = TIM #East Timor
			tag = TAI #Taiwan
			tag = HKG #Hong Kong
			tag = BRM #Myanmar
			tag = CBD #Cambodia
			tag = LAO #Laos
			tag = SIN #Singapore
			tag = BRU #Brunei
			tag = IND #Indonesia
			tag = PHI #Philippines
			tag = VIE #Vietnam
			tag = SIA #Thailand
			tag = MAY #Malaysia
			tag = SOL #Solomon Islands
			tag = MIC #Micronesia
		}
	}

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }

	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean"
		add_political_power = 50

		# Starts the Ascension Protocols
		if = { limit = { NOT = { has_idea = ASEAN_member }}
			set_country_flag = ASEAN_attempting_to_join
		}
	}
}

# Policies and Member Sections
# Military and Political Sections
shared_focus = {
	id = ASEAN_mutual_defense
	icon = little_entente
	cost = 10
	x = -5
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}
	select_effect = {
		set_global_flag = ASEAN_mutual_defense_proposing_flag
	}
	available = {
		NOT = {
			has_global_flag = ASEAN_mutual_defense_proposing_flag
			has_global_flag = ASEAN_active_voting
		}
	}
	bypass = {
		OR = {
			has_global_flag = mutual_defense_failed
			has_global_flag = mutual_defense_passed
		}
	}
	cancel = {
		has_global_flag = ASEAN_mutual_defense_proposing_flag
		NOT = { has_country_flag = ASEAN_started_mutual_defense_bill }
	}
	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_mutual_defense"
		set_global_flag = ASEAN_active_voting
		set_global_flag = ASEAN_mutual_defense_proposing_flag
		set_country_flag = ASEAN_started_mutual_defense_bill
		country_event = { id = asean_voting_events.3 days = 1 }
	}
}

shared_focus = { ##Not a Vote Focus
	id = ASEAN_seek_foreign_suppliers
	icon = lend_lease
	cost = 5
	x = -2
	y = 1
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_mutual_defense }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_seek_foreign_suppliers"
		add_war_support = 0.02
	}
}

shared_focus = {
	id = ASEAN_conduct_naval_exercises
	icon = naval_dockyard
	cost = 10
	x = 0
	y = 1
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_mutual_defense }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_conduct_naval_exercises"
		navy_experience = 25
	}
}

shared_focus = {
	id = ASEAN_greater_cooperation
	icon = coalition_army
	cost = 10
	x = 2
	y = 1
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_mutual_defense }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_greater_cooperation"
	}
}

shared_focus = {
	id = ASEAN_asean_forces
	icon = military_mission
	cost = 10
	x = -2
	y = 2
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_conduct_naval_exercises }
	prerequisite = { focus = ASEAN_seek_foreign_suppliers }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean_forces"
	}
}

shared_focus = {
	id = ASEAN_crush_the_cartels
	icon = city_fort
	cost = 10
	x = 0
	y = 2
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_conduct_naval_exercises }
	prerequisite = { focus = ASEAN_greater_cooperation }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean_forces"
	}
}

shared_focus = {
	id = ASEAN_expanded_military_cooperation
	icon = military_planning
	cost = 10
	x = 2
	y = 2
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_greater_cooperation }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_expanded_military_cooperation"
	}
}

shared_focus = {
	id = ASEAN_defense_accords
	icon = ultimatum
	cost = 10
	x = -2
	y = 3
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_forces }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_defense_accords"
	}
}

shared_focus = {
	id = ASEAN_joint_chief_of_staff
	icon = military_academy
	cost = 10
	x = 0
	y = 3
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_forces }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_joint_chief_of_staff"
	}
}

shared_focus = {
	id = ASEAN_military_investiture
	icon = industry_tanks
	cost = 10
	x = 2
	y = 3
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = {
		focus = ASEAN_asean_forces
		focus = ASEAN_expanded_military_cooperation
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_military_investiture"
	}
}

shared_focus = {
	id = ASEAN_shanghai_cooperation
	icon = align_to_china
	cost = 10
	x = -2
	y = 4
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_military_investiture }
	prerequisite = { focus = ASEAN_defense_accords }
	prerequisite = { focus = ASEAN_joint_chief_of_staff }
	mutually_exclusive = { focus = ASEAN_self_sufficiency }
	mutually_exclusive = { focus = ASEAN_american_agreements }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_shanghai_cooperation"
	}
}

shared_focus = {
	id = ASEAN_self_sufficiency
	icon = align_to_asean
	cost = 10
	x = 0
	y = 4
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_military_investiture }
	prerequisite = { focus = ASEAN_defense_accords }
	prerequisite = { focus = ASEAN_joint_chief_of_staff }
	mutually_exclusive = { focus = ASEAN_shanghai_cooperation }
	mutually_exclusive = { focus = ASEAN_american_agreements }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_self_sufficiency"
	}
}

shared_focus = {
	id = ASEAN_american_agreements
	icon = align_to_america
	cost = 10
	x = 2
	y = 4
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_military_investiture }
	prerequisite = { focus = ASEAN_defense_accords }
	prerequisite = { focus = ASEAN_joint_chief_of_staff }
	mutually_exclusive = { focus = ASEAN_shanghai_cooperation }
	mutually_exclusive = { focus = ASEAN_self_sufficiency }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_american_agreements"
	}
}

shared_focus = {
	id = ASEAN_chinese_weapon_contracts
	icon = small_arms_east
	cost = 10
	x = -2
	y = 5
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_shanghai_cooperation }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_chinese_weapon_contracts"
	}
}

shared_focus = {
	id = ASEAN_strength_in_asia
	icon = recruitment
	cost = 10
	x = 0
	y = 5
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = {  focus = ASEAN_self_sufficiency }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_strength_in_asia"
	}
}

shared_focus = {
	id = ASEAN_american_arms_deals
	icon = small_arms_west
	cost = 10
	x = 2
	y = 5
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_american_agreements }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_american_arms_deals"
	}
}

shared_focus = {
	id = ASEAN_chinese_training_routines
	icon = gentleman_officers
	cost = 10
	x = -2
	y = 6
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_chinese_weapon_contracts }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_chinese_training_routines"
	}
}

shared_focus = {
	id = ASEAN_reduce_foreign_dependence
	icon = military_fort
	cost = 10
	x = 0
	y = 6
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_strength_in_asia }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_reduce_foreign_dependence"
	}
}

shared_focus = {
	id = ASEAN_american_doctrine
	icon = legacy_of_marines
	cost = 10
	x = 2
	y = 6
	relative_position_id = ASEAN_mutual_defense

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_MILITARY_LAWS FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_american_arms_deals }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_american_doctrine"
	}
}

###Economic Shared Focus
shared_focus = {
	id = ASEAN_free_trade_agreements
	icon = asean_mutual_trade
	cost = 10
	x = 0
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = {
		focus = ASEAN_asean
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_free_trade_agreements"
	}
}

shared_focus = {
	id = ASEAN_revise_the_cept
	icon = liberalization
	cost = 10
	x = -1
	y = 1
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_free_trade_agreements }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_revise_the_cept"
	}
}

shared_focus = {
	id = ASEAN_establish_an_financial_net
	icon = welfare
	cost = 10
	x = 1
	y = 1
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_free_trade_agreements }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_establish_an_financial_net"
	}
}

shared_focus = {
	id = ASEAN_zero_tariff_agreements
	icon = export_economy
	cost = 10
	x = -1
	y = 2
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_revise_the_cept }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_zero_tariff_agreements"
	}
}

shared_focus = {
	id = ASEAN_economic_cooperation
	icon = industry2
	cost = 10
	x = 1
	y = 2
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_establish_an_financial_net }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_economic_cooperation"
	}
}

shared_focus = {
	id = ASEAN_common_currency
	icon = coins
	cost = 10
	x = 0
	y = 3
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_zero_tariff_agreements }
	prerequisite = { focus = ASEAN_economic_cooperation }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_economic_cooperation"
	}
}

shared_focus = {
	id = ASEAN_centralized_market
	icon = centralization
	cost = 10
	x = -1
	y = 4
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_common_currency }
	mutually_exclusive = { focus = ASEAN_decentralized_market }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_economic_cooperation"
	}
}

shared_focus = {
	id = ASEAN_decentralized_market
	icon = decentralized_economy
	cost = 10
	x = 1
	y = 4
	relative_position_id = ASEAN_free_trade_agreements

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_common_currency }
	mutually_exclusive = { focus = ASEAN_centralized_market }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_decentralized_market"
	}
}

shared_focus = {
	id = ASEAN_found_the_asean_court
	icon = generic_government
	cost = 10
	x = 2
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_decentralized_market"
	}
}

shared_focus = {
	id = ASEAN_joint_intelligence_operations
	icon = recon
	cost = 10
	x = 4
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_decentralized_market"
	}
}

##ASEAN and Her Neighbors
shared_focus = {
	id = ASEAN_asean_and_her_neighbors
	icon = align_to_asean
	cost = 10
	x = 6
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean_and_her_neighbors"
	}
}

shared_focus = {
	id = ASEAN_asean_plus_three
	icon = political_pressure
	cost = 10
	x = 4
	y = 2
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_and_her_neighbors }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean_plus_three"
	}
}

shared_focus = {
	id = ASEAN_global_community
	icon = internationalism
	cost = 10
	x = 6
	y = 2
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_and_her_neighbors }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_global_community"
	}
}

shared_focus = {
	id = ASEAN_asean_plus_six
	icon = political_pressure
	cost = 10
	x = 8
	y = 2
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_and_her_neighbors }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean_plus_six"
	}
}

shared_focus = {
	id = ASEAN_sino_asean_treaties
	icon = align_to_china
	cost = 10
	x = 3
	y = 3
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_plus_three }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_asean_plus_three"
	}
}

shared_focus = {
	id = ASEAN_korean_asean_cooperation
	icon = align_to_south_korea
	cost = 10
	x = 5
	y = 3
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_plus_three }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_korean_asean_cooperation"
	}
}

shared_focus = {
	id = ASEAN_japanese_treaties
	icon = align_to_japan
	cost = 10
	x = 4
	y = 4
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_plus_three }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_korean_asean_cooperation"
	}
}

shared_focus = {
	id = ASEAN_indian_cooperation
	icon = align_to_india
	cost = 10
	x = 7
	y = 3
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_plus_six }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_indian_cooperation"
	}
}

shared_focus = {
	id = ASEAN_australian_treaties
	icon = align_to_australia
	cost = 10
	x = 9
	y = 3
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_plus_six }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_australian_treaties"
	}
}

shared_focus = {
	id = ASEAN_new_zealand_discussion
	icon = align_to_new_zealand
	cost = 10
	x = 8
	y = 4
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean_plus_six }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_new_zealand_discussion"
	}
}

shared_focus = {
	id = ASEAN_political_preparation
	icon = treaty2
	cost = 10
	x = 8
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_political_preparation"
	}
}

##Joining the ASEAN
shared_focus = {
	id = ASEAN_ascension_protocols
	icon = accept_treaty
	cost = 10
	x = 12
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_ascension_protocols"
	}
}

shared_focus = {
	id = ASEAN_prepare_the_associations
	icon = intelligence_exchange
	cost = 10
	x = -1
	y = 1
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_ascension_protocols }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_prepare_the_associations"
	}
}

shared_focus = {
	id = ASEAN_formalize_the_guidelines
	icon = treaty
	cost = 10
	x = 1
	y = 1
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_ascension_protocols }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_formalize_the_guidelines"
	}
}

shared_focus = {
	id = ASEAN_south_asian_expansion
	icon = molotov_ribbentrop_pact
	cost = 10
	x = 0
	y = 2
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_formalize_the_guidelines }
	prerequisite = { focus = ASEAN_prepare_the_associations }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_south_asian_expansion"
	}
}

shared_focus = {
	id = ASEAN_papua_new_guinea
	icon = align_to_papua_new_guinea
	cost = 10
	x = -2
	y = 3
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_south_asian_expansion }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_papua_new_guinea"
	}
}

shared_focus = {
	id = ASEAN_east_timor
	icon = align_to_east_timor
	cost = 10
	x = 0
	y = 3
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_south_asian_expansion }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_east_timor"
	}
}

shared_focus = {
	id = ASEAN_bangladesh
	icon = align_to_bangladesh
	cost = 10
	x = 2
	y = 3
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_south_asian_expansion }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_bangladesh"
	}
}

shared_focus = {
	id = ASEAN_look_to_the_horizon
	icon = improve_relations_blue
	cost = 10
	x = 0
	y = 4
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_bangladesh }
	prerequisite = { focus = ASEAN_east_timor }
	prerequisite = { focus = ASEAN_papua_new_guinea }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_look_to_the_horizon"
	}
}

shared_focus = {
	id = ASEAN_the_pacific_islands
	icon = oceania
	cost = 10
	x = -1
	y = 5
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_look_to_the_horizon }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_the_pacific_islands"
	}
}

shared_focus = {
	id = ASEAN_sri_lanka
	icon = align_to_sri_lanka
	cost = 10
	x = 1
	y = 5
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_look_to_the_horizon }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_sri_lanka"
	}
}

shared_focus = {
	id = ASEAN_australia
	icon = align_to_australia
	cost = 10
	x = -1
	y = 6
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_sri_lanka }
	prerequisite = { focus = ASEAN_the_pacific_islands }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_sri_lanka"
	}
}

shared_focus = {
	id = ASEAN_new_zealand
	icon = align_to_new_zealand
	cost = 10
	x = 1
	y = 6
	relative_position_id = ASEAN_ascension_protocols

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		has_idea = ASEAN_Member
	}

	prerequisite = { focus = ASEAN_sri_lanka }
	prerequisite = { focus = ASEAN_the_pacific_islands }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_sri_lanka"
	}
}

# ASEAN Joining Protocols
#
shared_focus = {
	id = ASEAN_look_to_the_asean
	icon = align_to_asean
	cost = 10
	x = 0
	y = 1
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		NOT = {	has_idea = ASEAN_Member }
	}

	prerequisite = { focus = ASEAN_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_look_to_the_asean"
	}
}

shared_focus = {
	id = ASEAN_asean_associate_state
	icon = treaty2
	cost = 10
	x = 0
	y = 2
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		NOT = {	has_idea = ASEAN_Member }
	}

	prerequisite = { focus = ASEAN_look_to_the_asean }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_look_to_the_asean"
	}
}

shared_focus = {
	id = ASEAN_member_state
	icon = align_to_asean
	cost = 10
	x = 0
	y = 3
	relative_position_id = ASEAN_asean

	cancel_if_invalid = yes
	continue_if_invalid = yes

	search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_ASEAN }
	allow_branch = {
		NOT = {	has_idea = ASEAN_Member }
	}

	prerequisite = { focus = ASEAN_asean_associate_state }
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: focus ASEAN_look_to_the_asean"
	}
}