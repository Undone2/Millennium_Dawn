leader_traits = {
	ARAB_knows_best_ASSADISM = {
		random = no
		communism_drift = 0.05
		political_power_factor = 0.05
		emerging_outlook_campaign_cost_modifier = -0.1
		corruption_cost_factor = 0.25
	}
	ARAB_personality_cult_ASSADISM = {
		random = no
		communism_drift = 0.1
		political_power_factor = 0.1
		emerging_outlook_campaign_cost_modifier = -0.2
		corruption_cost_factor = 0.50
	}






	ARAB_patriotic_educator_SADDAMISM = {
		random = no
		nationalist_drift = 0.1
		education_cost_multiplier_modifier = 0.05
		nationalist_outlook_campaign_cost_modifier = -0.1
		corruption_cost_factor = 0.3
	}
	ARAB_anti_marxist_SADDAMISM = {
		random = no
		nationalist_drift = 0.2
		education_cost_multiplier_modifier = 0.05
		nationalist_outlook_campaign_cost_modifier = -0.2
		corruption_cost_factor = 0.75
	}
}