TAI_category = {
	TAI_coast_guard = {
		allowed = { tag = TAI }
		icon = GFX_decision_generic_naval

		cost = 25

		available = {
			date > 2000.2.1
		}
		highlight_states = {
			highlight_states_trigger = {
				state = 598
			}
		}

		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision TAI_coast_guard"
			effect_tooltip = { navy_experience = 5 }
			hidden_effect = { country_event = { id = taiwan.4 days = 1 } }
		}

		ai_will_do = {
			factor = 1
		}
	}
	TAI_kaoshiung_university = {
		allowed = { tag = TAI }
		icon = GFX_decision_generic_research

		cost = 100

		available = {
			date > 2000.2.1
		}
		highlight_states = {
			highlight_states_trigger = {
				state = 599
			}
		}

		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision TAI_kaoshiung_university"
			add_tech_bonus = {
				name = TAI_kaoshiung_university
				bonus = 0.2
				uses = 2
				category = computing_tech
			}
			add_tech_bonus = {
				name = TAI_kaoshiung_university
				bonus = 0.3
				uses = 2
				category = industry
			}
			set_temp_variable = { treasury_change = -5.00 }
			modify_treasury_effect = yes
			hidden_effect = { country_event = { id = taiwan.5 days = 1 } }
		}

		ai_will_do = {
			factor = 1
		}
	}	
}

TAI_economy_decisions_category = {
	 TAI_consumer_voucher_policy = {
		  visible = {  TAG = TAI  }
		  available = {
			   has_stability > 0.45
			   has_war = no
		  }
		  cost = 100
		  icon = GFX_decision_ger_mefo_bills
		  fire_only_once = yes
		  complete_effect = {
		  log = "[GetDateText]: [Root.GetName]: Decision TAI_consumer_voucher_policy"
			   add_stability = 0.1	
		  }
	 }
	 TAI_industrial_develop_project = {
		  visible = {  TAG = TAI  }
		  available = {
			   has_stability > 0.65
			   has_war = no
		  }
		  
		  icon = GFX_decision_ger_mefo_bills
		  days_re_enable = 180
		  complete_effect = {
			   log = "[GetDateText]: [Root.GetName]: Decision TAI_industrial_develop_project"
			   add_offsite_building = { type = industrial_complex level = 1 }	
		  }
	 }
	 TAI_han_kuang_exercise_propaganda = {
		  visible = {  TAG = TAI  }
		  available = {
			   has_stability > 0.45
			   has_war = no
		  }
		  cost = 20
		  icon = GFX_decision_generic_army_support
		  days_re_enable = 360
		  complete_effect = {
			   log = "[GetDateText]: [Root.GetName]: Decision TAI_han_kuang_exercise_propaganda"
			   add_war_support = 0.05
		  }
	 }
	 TAI_construction_policy = {
		  visible = {  TAG = TAI  }
		  available = {
			   has_stability > 0.25
		  }
		  cost = 20
		  icon = GFX_decision_generic_taiwan
		  days_re_enable = 360
		  complete_effect = {
		  log = "[GetDateText]: [Root.GetName]: Decision TAI_construction_policy"
		  add_war_support = 0.001
		  add_stability = 0.001
		  random_owned_state = {
			add_extra_state_shared_building_slots = 4	
			}
		 }
	}
	TAI_weapon_self_made_program = {
		  visible = {  TAG = TAI  }
		  available = {
			   has_stability > 0.75
		  }
		  
		  icon = GFX_decision_generic_taiwan
		  days_re_enable = 360
		  complete_effect = {
		  log = "[GetDateText]: [Root.GetName]: Decision TAI_weapon_self_made_program"		  
		  add_offsite_building = { type = arms_factory level = 1 }
		  }
	 }
}

TAI_mobilization_decisions_category = {

TAI_set_up_afrc_units = {
		 visible = {
			 has_completed_focus = TAI_expand_reserve_force_members
		  }
		  cost = 0
		  available = {
		 
		  }
		  
		  icon = GFX_decision_category_army_reform
		  
		 fire_only_once = yes
		  
		complete_effect = {
		log = "[GetDateText]: [Root.GetName]: Decision TAI_set_up_afrc_units" 
		set_country_flag = TAI_afrc_units_were_all_set
		  
		division_template = {
		name = "Reserve Armored Infantry Brigade"
		regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		L_arm_Bat = { x = 1 y = 1 }
		L_arm_Bat = { x = 1 y = 2 }
		}
		support = { 
		L_Engi_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		} 
		}
		
		division_template = {
		name = "Mobilized Reserve Infantry"
		regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		L_arm_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 2 }
		}
		support = { 
		Mech_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 0 }
		} 
		}
		division_template = {
		name = "Mobilized basic Infantry"
		regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		L_Inf_Bat = { x = 1 y = 3 }
		L_Inf_Bat = { x = 2 y = 0 }
		L_Inf_Bat = { x = 2 y = 1 }
		L_Inf_Bat = { x = 2 y = 2 }
		L_Inf_Bat = { x = 2 y = 3 }
		}
		support = { 
		L_Engi_Comp = { x = 0 y = 0 }
		} 
		} 
		set_division_template_lock = {
		division_template = "Reserve Armored Infantry Brigade"
		is_locked = yes
		 }
		set_division_template_lock = {
		division_template = "Mobilized Reserve Infantry"
		is_locked = yes
		  }
		set_division_template_lock = {
		division_template = "Mobilized basic Infantry"
		is_locked = yes
		 }
		  }
}


TAI_reserve_army_first_level_mobilization = {
visible = {
			has_completed_focus = TAI_first_level_mobilization
		  }
		  cost = 0
		  available = {
		  AND = { has_country_flag = TAI_afrc_units_were_all_set
		  NOT = { has_country_flag = TAI_mob_first_level }
		  OR = {
		  has_war = yes 
		  has_completed_focus = TAI_mobilized_reserve_troops
			 }
		}
		  }
		  
		  icon = armed_forces_officers
		  days_re_enable = 0
		  
		complete_effect = {
		log = "[GetDateText]: [Root.GetName]: Decision TAI_reserve_army_first_level_mobilization"
		set_country_flag = TAI_mob_first_level
		599 = {
			create_unit = {
		 division = "name = \"Gaoxiong District Reserve Armored Infantry Brigade\" division_template = \"Reserve Armored Infantry Brigade\" start_experience_factor = 0.3 start_equipment_factor = 1"
		 owner = TAI
		}
		
		}
		598 = {
			create_unit = {
		 division = "name = \"Taipei District First Reserve Armored Infantry Brigade\" division_template = \"Reserve Armored Infantry Brigade\" start_experience_factor = 0.3 start_equipment_factor = 1"
		 owner = TAI
		}
		
		}
		
		600 = {
			create_unit = {
		 division = "name = \"Hualian District Reserve Armored Infantry Brigade\" division_template = \"Reserve Armored Infantry Brigade\" start_experience_factor = 0.3 start_equipment_factor = 1"
		 owner = TAI
		}
		
		}
		  
		}
}

TAI_reserve_army_northern_district_second_level_mobilization = {
		 visible = {
			 has_completed_focus = TAI_northern_district_second_level_mobilization
		  }
			  available = {
			AND = {
			 has_country_flag = TAI_afrc_units_were_all_set
			 has_country_flag = TAI_mob_first_level
			 NOT = { has_country_flag = TAI_mob_north_second_level } 
			OR = {
		  AND = { has_war = yes has_war_support > 0.5 }
		  has_completed_focus = TAI_mobilized_reserve_troops
		}
		  }  
		  }
		  icon = armed_forces_officers
		  days_re_enable = 0
		 complete_effect = {
		 log = "[GetDateText]: [Root.GetName]: Decision TAI_reserve_army_northern_district_second_level_mobilization"
		 set_country_flag = TAI_mob_north_second_level
		 1155 = {
			create_unit = {
		 division = "name = \"Northern Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 7214
		}
			create_unit = {
		 division = "name = \"Northern Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		  prioritize_location = 14412
		}
		}
		   }
}
TAI_reserve_army_central_district_second_level_mobilization = {
		 visible = {
			 has_completed_focus = TAI_central_district_second_level_mobilization
		  }
			  available = {
			AND = {
			 has_country_flag = TAI_afrc_units_were_all_set
			 has_country_flag = TAI_mob_first_level
			OR = {
		  AND = { has_war = yes has_war_support > 0.5 }
		  has_completed_focus = TAI_mobilized_reserve_troops
		}
		 NOT = { has_country_flag = TAI_mob_central_second_level }
		  }  }
		  icon = armed_forces_officers
		  days_re_enable = 0
		complete_effect = {
		log = "[GetDateText]: [Root.GetName]: Decision TAI_reserve_army_central_district_second_level_mobilization"
		set_country_flag = TAI_mob_central_second_level
		1156 = {
		create_unit = {
		 division = "name = \"Central Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14411
		}
			create_unit = {
		 division = "name = \"Central Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 11959
		}		
		}
		   }
}

TAI_reserve_army_southern_district_second_level_mobilization = {
		 visible = {
			 has_completed_focus = TAI_southern_district_second_level_mobilization
		  }
			available = {
			AND = { has_country_flag = TAI_afrc_units_were_all_set
					has_country_flag = TAI_mob_first_level
			NOT = { has_country_flag = TAI_mob_south_second_level }
			 
			OR = {
		  AND = { has_war = yes has_war_support > 0.5 }
		  has_completed_focus = TAI_mobilized_reserve_troops
		}
		  }  
		  }
		  icon = armed_forces_officers
		  days_re_enable = 0
		complete_effect = {
		log = "[GetDateText]: [Root.GetName]: Decision TAI_reserve_army_southern_district_second_level_mobilization"
		set_country_flag = TAI_mob_south_second_level
		1157 = {
		create_unit = {
		 division = "name = \"Southern Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 1091
		}
		create_unit = {
		 division = "name = \"Southern Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 12027
		}
		create_unit = {
		 division = "name = \"Southern Mobilized Reserve Infantry\" division_template = \"Mobilized Reserve Infantry\" start_experience_factor = 0.1 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 12068
		}
		}
		   }
}
TAI_total_mobilization_decision = {
		 visible = {
			 has_completed_focus = TAI_total_mobilization
		  }
			available = {
			AND = {
			NOT = { has_country_flag = TAI_total_mobilized }
			 
			 AND = { 
			 has_country_flag = TAI_afrc_units_were_all_set
			 has_country_flag = TAI_mob_south_second_level
			 has_country_flag = TAI_mob_north_second_level
			 has_country_flag = TAI_mob_central_second_level
			 }
			OR = {
		  AND = { has_war = yes has_war_support > 0.8 }
		  AND = { has_completed_focus = TAI_mobilized_reserve_troops has_war_with = CHI }
		}
		   }
		  }
		  icon = armed_forces_officers
		  days_re_enable = 0
		complete_effect = {
		log = "[GetDateText]: [Root.GetName]: Decision TAI_total_mobilization_decision"
		set_country_flag = TAI_total_mobilized 
		
		430 = {
		
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13275
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13275
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13275
		}
		}
		1109 = {
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 14608
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 14608
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 14608
		}
		}
		1158 = {
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13274
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13274
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13274
		}
		}
		1015 = {
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13326
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13326
		}
		create_unit = {
		 division = "name = \"Mobilized Militia Infantry\" division_template = \"Mobilized basic Infantry\" start_experience_factor = 0.05 start_equipment_factor = 0.9"
		 owner = TAI
		 prioritize_location = 13326
		}
		}
		   }
}

TAI_demobilization = {
		 visible = {
		 OR = 
			{ has_completed_focus = TAI_first_level_mobilization
			  has_completed_focus = TAI_northern_district_second_level_mobilization
			  has_completed_focus = TAI_central_district_second_level_mobilization
			  has_completed_focus = TAI_southern_district_second_level_mobilization
			  has_completed_focus = TAI_total_mobilization
			}
		  }
		  cost = 0
		  available = {
		  NOT = { has_war = yes }
		  OR = {
		  has_country_flag = TAI_mob_first_level
		  has_country_flag = TAI_mob_south_second_level
		  has_country_flag = TAI_mob_north_second_level
		  has_country_flag = TAI_mob_central_second_level
		  has_country_flag = TAI_total_mobilized
		  }
		  }
		  
		  icon = armed_forces_officers
		  days_re_enable = 0
		  
		  complete_effect = {
		  log = "[GetDateText]: [Root.GetName]: Decision TAI_demobilization"
		  delete_unit_template_and_units = {
		  division_template = "Reserve Armored Infantry Brigade"
					 }
					 delete_unit_template_and_units = {
		  division_template = "Mobilized Reserve Infantry"
					 }
					 delete_unit_template_and_units = {
		  division_template = "Mobilized basic Infantry"
					 }
					 
		clr_country_flag = TAI_total_mobilized
		clr_country_flag = TAI_mob_south_second_level
		clr_country_flag = TAI_mob_north_second_level
		clr_country_flag = TAI_mob_central_second_level
		clr_country_flag = TAI_mob_first_level
		
		
		division_template = {
		name = "Reserve Armored Infantry Brigade"
		regiments = {
		armor_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		L_arm_Bat = { x = 1 y = 1 }
		L_arm_Bat = { x = 1 y = 2 }
		}
		support = { 
		L_Engi_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		} 
		}
		
		division_template = {
		name = "Mobilized Reserve Infantry"
		regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		L_arm_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 2 }
		}
		support = { 
		Mech_Recce_Comp = { x = 0 y = 2 }
		combat_service_support_company = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 0 }
		} 
		}
		division_template = {
		name = "Mobilized basic Infantry"
		regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 1 y = 0 }
		L_Inf_Bat = { x = 1 y = 1 }
		L_Inf_Bat = { x = 1 y = 2 }
		L_Inf_Bat = { x = 1 y = 3 }
		L_Inf_Bat = { x = 2 y = 0 }
		L_Inf_Bat = { x = 2 y = 1 }
		L_Inf_Bat = { x = 2 y = 2 }
		L_Inf_Bat = { x = 2 y = 3 }
		}
		support = { 
		L_Engi_Comp = { x = 0 y = 0 }
		} 
		} 
		set_division_template_lock = {
		division_template = "Reserve Armored Infantry Brigade"
		is_locked = yes
		 }
		set_division_template_lock = {
		division_template = "Mobilized Reserve Infantry"
		is_locked = yes
		  }
		set_division_template_lock = {
		division_template = "Mobilized basic Infantry"
		is_locked = yes
		 }  
		
		}
		 }
}

TAI_png_decisions_category = {
TAI_png_launch = {
		  visible = {  TAG = TAI   }
		  available = {
		  has_completed_focus = TAI_launch_our_plan
		  }
		cost = 10
		icon = intervention_in_china
		fire_only_once = yes
		complete_effect = {
		log = "[GetDateText]: [Root.GetName]: Decision TAI_png_launch"
		army_experience = 60
		if = {
		limit = { has_country_flag = TAI_army_1sf }
				TAI = {
		set_state_controller = 925
		}
		division_template = {
		name = "National Revolutionary Army Special Force"
		regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 2 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }
		Mot_Inf_Bat = { x = 2 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 2 }
		}
		support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 4 }
		Arty_Battery = { x = 0 y = 3 }
		SP_AA_Battery = { x = 0 y = 2 }
		}
		priority = 2
		}
		division_template = {
		name = "National Revolutionary Army Marine Special Force"
		regiments = {
		L_Marine_Bat = { x = 1 y = 0 }
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 1 y = 1 }
		L_Marine_Bat = { x = 1 y = 2 }
		L_Marine_Bat = { x = 0 y = 1 }
		L_Marine_Bat = { x = 0 y = 2 }
		}
		support = { 
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		} 
		priority = 2
		}
		division_template = {
		name = "National Revolutionary Army Airborne Special Force"
		regiments = {
		L_Air_assault_Bat = { x = 1 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 1 y = 1 }
		L_Air_assault_Bat = { x = 1 y = 2 }
		L_Air_assault_Bat = { x = 0 y = 1 }
		L_Air_assault_Bat = { x = 0 y = 2 }
		}
		support = { 
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
		} 
		priority = 2
		}
		
		598 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Marine\" division_template = \"National Revolutionary Army Marine Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 7180
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14699
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14701
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14702
		}
		}
		
		599 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 11914
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14410
		}
		
			create_unit = {
		 division = "name = \"National Revolutionary Army Marine\" division_template = \"National Revolutionary Army Marine Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14410
		}
			
		}
		430 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 13275
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 13275
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 13275
		}
		}
		600 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 4096
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 1175
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 9955
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Airborne\" division_template = \"National Revolutionary Army Airborne Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14698
		}
		}
		1109 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14608
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14608
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14608
		}
		}
		1155 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 7214
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14412
		}
		}
		1156 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 11959
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 14411
		}
		}
		1157 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 1091
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 12027
		}
		   create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 12068
		}
		}
		1158 = {
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 13274
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 13274
		}
			create_unit = {
		 division = "name = \"National Revolutionary Army Special Force\" division_template = \"National Revolutionary Army Special Force\" start_experience_factor = 0.9 start_equipment_factor = 1"
		 owner = TAI
		 prioritize_location = 13274
		}
		}
		
		TAI = {
		declare_war_on = {
		target = CHI
		type = annex_everything }
		}
		declare_war_on = {
		target = MON
		type = annex_everything }
		}
		
		}
	 }
}