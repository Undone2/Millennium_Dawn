GER_gdr_proxy_conflict_decisions_category = {
    GER_financial_assistance = {
        icon = GFX_decision_generic_join_nato
		fire_only_once = no
		days_remove = 20
		available = {
		}
        visible = {
                has_idea = NATO_member
                NOT = {
                    original_tag = GER
                }
        }
		cost = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                limit = {
                    has_ideas = NATO_member
                }
                    set_temp_variable = { treasury_change = -15 }
			        modify_treasury_effect = yes
                    GER = {
                        set_temp_variable = { percent_change = 0.06 }
                        set_temp_variable = { tag_index = ROOT }
                        set_temp_variable = { influence_target = GER }
                        change_influence_percentage = yes
                    }

                GER = {
                    set_temp_variable = { treasury_change = 15 }
			        modify_treasury_effect = yes
                }
		}
	}

    GER_gdr_financial_assistance = {
        icon = GFX_decision_russian_money
		fire_only_once = no
		days_remove = 20
		available = {
		}
        visible = {
                has_idea = SOV_warsaw_pact_idea
        }
		cost = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
            if = {
                limit = {
                    has_idea = SOV_warsaw_pact_idea
                }
                    set_temp_variable = { treasury_change = -15 }
			        modify_treasury_effect = yes
                every_country = {
                    limit = {
                        has_country_flag = GDR_country
                    }
                    set_temp_variable = { treasury_change = 15 }
			        modify_treasury_effect = yes
                }
            }
		}
	}

    GER_ask_weapon_assistance = {
        icon = GFX_decision_generic_join_nato
		fire_only_once = no
		days_remove = 20
		available = {
		}
        visible = {
            original_tag = GER
            NOT = {
                has_country_flag = GDR_country
            }
        }
		cost = 75
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GER_ask_weapon_assistance"
                every_country = {
                    limit = {
                        has_idea = NATO_member
                    }
                    country_event = sov_warsaw_pact.17
                }
            }
		}

    GER_gdr_ask_weapon_assistance = {
        icon = GFX_warsaw_button
		fire_only_once = no
		days_remove = 20
		available = {
		}
        visible = {
            every_country = {
                limit = {
                    has_country_flag = GDR_country
                }
            }
            NOT = {
                has_idea = NATO_member
                has_idea = SOV_warsaw_pact_idea
            }
        }
		cost = 75
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision GER_gdr_ask_weapon_assistance"
                every_country = {
                    limit = {
                        has_idea = SOV_warsaw_pact_idea
                    }
                    country_event = sov_warsaw_pact.17
                }
            }
		}

        GER_efforts_for_a_peaceful_resolution_of_the_conflict = {
            icon = GFX_decision_hol_war_on_pacifism
            fire_only_once = no
            days_remove = 20
            available = {
            }
            visible = {
                original_tag = USA
                has_idea = NATO_member
            }
            cost = 45
            remove_effect = {
                log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                    limit = {
                        has_idea = NATO_member
                    }
                    GER = {
                        add_war_support = -0.04
                        add_stability = 0.02
                    }
            }
        }

        GER_gdr_efforts_for_a_peaceful_resolution_of_the_conflict = {
            icon = GFX_decision_hol_war_on_pacifism
            fire_only_once = no
            days_remove = 20
            available = {
            }
            visible = {
                original_tag = SOV
                has_idea = SOV_warsaw_pact_idea
            }
            cost = 45
            remove_effect = {
                log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                    limit = {
                        has_idea = SOV_warsaw_pact_idea
                    }
                        every_country = {
                            limit = {
                                has_country_flag = GDR_country
                            }
                            add_war_support = -0.04
                            add_stability = 0.02
                        }
            }
        }

        GER_Promote_a_just_end_to_the_conflict = {
            icon = GFX_decision_generic_intelligence_operation
            fire_only_once = no
            days_remove = 20
            available = {
            }
            visible = {
                original_tag = USA
                has_idea = NATO_member
            }
            cost = 45
            remove_effect = {
                log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                    limit = {
                        has_idea = NATO_member
                    }
                    GER = {
                        add_war_support = 0.04
                        add_stability = -0.02
                    }
            }
        }

        GER_gdr_Promote_a_just_end_to_the_conflict = {
            icon = GFX_decision_generic_intelligence_operation
            fire_only_once = no
            days_remove = 20
            available = {
            }
            visible = {
                    original_tag = SOV
                    has_idea = SOV_warsaw_pact_idea
            }
            cost = 45
            remove_effect = {
                log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                limit = {
                    has_idea = SOV_warsaw_pact_idea
                }
                    every_country = {
                        limit = {
                            has_country_flag = GDR_country
                        }
                        add_war_support = 0.04
                        add_stability = -0.02
                    }
            }
        }

        GER_Information_war_against_the_GDR = {
            icon = GFX_zsr_propaganda
            fire_only_once = no
            days_remove = 20
            available = {
            }
            visible = {
                original_tag = GER
                NOT = {
                    has_country_flag = GDR_country
                }
            }
            cost = 45
            remove_effect = {
                log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                    every_country = {
                        limit = {
                            has_country_flag = GDR_country
                        }
                        add_war_support = -0.02
                        add_stability = -0.01
                    }
            }
        }

        GER_Information_war_against_the_FRG = {
            icon = GFX_decision_frg
            fire_only_once = no
            days_remove = 20
            available = {
            }
            visible = {
                every_country = {
                    limit = {
                        has_country_flag = GDR_country
                    }
                }
                NOT = {
                    has_idea = NATO_member
                    has_idea = SOV_warsaw_pact_idea
                }
            }
            cost = 45
            remove_effect = {
                log = "[GetDateText]: [Root.GetName]: Decision GER_counter_terrorist_operation_decisions_category"
                    GER = {
                        add_war_support = -0.02
                        add_stability = -0.01
                    }
            }
        }
    }