INFANTRY
Militia_Bat
L_Engi_Bat
L_Inf_Bat
L_Marine_Bat
Special_Forces
L_Air_Inf_Bat
L_Air_assault_Bat
Arty_Bat
L_Recce_Bat

MOBILE
Mot_Militia_Bat
Mot_Inf_Bat
Mot_Marine_Bat
Mot_Recce_Bat
Mot_Air_Inf_Bat

ARMORED
H_Engi_Bat
Mech_Inf_Bat (apc)
Mech_Recce_Bat (apc)
SP_Arty_Bat
SP_AA_Bat
armor_Bat
Arm_Inf_Bat (ifv)
Arm_Recce_Bat (ifv)
armor_Recce_Bat (recon tank)
Arm_Air_assault_Bat
Mech_Marine_Bat
Arm_Marine_Bat

SUPPORT COMPANIES
armor_Comp
L_Recce_Comp
L_Engi_Comp
Mot_Recce_Comp
Mech_Recce_Comp (apc)
Arm_Recce_Comp (ifv)
armor_Recce_Comp (recon tanks)
H_Engi_Comp
Arty_Battery
SP_Arty_Battery
SP_AA_Battery