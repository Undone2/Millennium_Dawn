leader_traits = {

	endorsed_company_mobile_phone_manufacturer = {
		random = no
		
		ai_will_do = {
			factor = 1
		}
	}
	
	endorsed_company_oil_company = {
		random = no
		
		ai_will_do = {
			factor = 1
		}
	}
	
	endorsed_company_logging_company = {
		random = no
		
		ai_will_do = {
			factor = 1
		}
	}
	
	endorsed_company_armoured_vehicles = {
		random = no
		
		equipment_bonus = {
			APC_Equipment = {
				armor_value = 0.15
				soft_attack = 0.15
			}
			IFV_Equipment = {
				armor_value = 0.15
				soft_attack = 0.15
			}
			util_vehicle_equipment = {
				armor_value = 0.15
				soft_attack = 0.15
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
}