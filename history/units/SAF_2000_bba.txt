﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		variant_name = "Cheetah"
		amount = 39
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Impala"
		amount = 48
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Pilatus PC-7"
		amount = 58
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 16
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Dassault Falcon 20"
		amount = 3
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = large_plane_maritime_patrol_airframe_1
		variant_name = "Br.1150 Atlantic"				#in place of C-47
		amount = 5
		producer = FRA
	}
}