﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter2		#F-16 Fighting falcon
		amount = 24
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#Alpha Jet
		amount = 40
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1		#C-130
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #P-3C Orion
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-316 Alouette III
		amount = 28
		producer = FRA
	}
}