﻿division_template = {
	name = "Mechanized Brigade"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Air Assault Brigade"

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Counter Guerrilla Force"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		Special_Forces = { x = 1 y = 0 }
		Special_Forces = { x = 1 y = 1 }
	}
}
division_template = {
	name = "Infantry Division" #While the various Colombian Divisions have different makeups, the differences are usually regiments not represented by the mod, so I made one for most of them.

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Special_Forces = { x = 0 y = 3 }
		Arty_Bat = { x = 0 y = 4 }
		Special_Forces = { x = 1 y = 0 }
		Special_Forces = { x = 1 y = 1 }
		Special_Forces = { x = 1 y = 2 }
		Arty_Bat = { x = 1 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "1st Mechanized Brigade"
		location = 8056
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Mechanized Brigade"
		location = 7993
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Mechanized Brigade"
		location = 7936
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Mechanized Brigade"
		location = 10553
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "5th Mechanized Brigade"
		location = 10747
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "6th Mechanized Brigade"
		location = 2007
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "1st Air Assault Brigade"
		location = 10251
		division_template = "Air Assault Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "2nd Air Assault Brigade"
		location = 10792
		division_template = "Air Assault Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "1st Infantry Division"
		location = 11469
		division_template = "Infantry Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Infantry Division"
		location = 1887
		division_template = "Infantry Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Infantry Division"
		location = 10637
		division_template = "Infantry Division"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "1st Mobile Counter Guerrilla Force (bde)"
		location = 2007
		division_template = "Counter Guerrilla Force"
		start_experience_factor = 0.65
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Mobile Counter Guerrilla Force (bde)"
		location = 10253
		division_template = "Counter Guerrilla Force"
		start_experience_factor = 0.65
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Mobile Counter Guerrilla Force (bde)"
		location = 12729
		division_template = "Counter Guerrilla Force"
		start_experience_factor = 0.65
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 1600
		#producer = USA
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1
		amount = 15000
		#producer = ISR
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2
		amount = 750
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 120
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 700
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 500
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 127
		producer = BRA
		variant_name = "EE-9 Cascavel"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M113"
		amount = 80
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "E-11 Urutu"
		amount = 76
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_4 #RG-31 Nyala
		amount = 4
		producer = SAF
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M101
		amount = 130
		producer = USA
	}
}