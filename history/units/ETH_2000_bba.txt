﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aero L-39"
		type = small_plane_strike_airframe_1
		amount = 10
		producer = CZE
	}
	add_equipment_to_stockpile = {
		variant_name = "SIAI-Marchetti SF.260"
		type = small_plane_strike_airframe_1
		amount = 10
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-21s Bis"
		type = small_plane_strike_airframe_1
		amount = 24
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-23"
		type = small_plane_strike_airframe_1
		amount = 17
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-25"
		type = medium_plane_cas_airframe_2
		amount = 4
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-27"
		type = medium_plane_fighter_airframe_2
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-12"
		amount = 7
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "DHC-4 Caribou"				#in place of DHC-6's
		amount = 2
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Harbin Y-12"
		amount = 2
		producer = CHI
	}
}