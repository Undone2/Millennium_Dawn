﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Mirage 5"
		type = small_plane_strike_airframe_1
		amount = 9
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"				#additional units to replace L-100-30 aircraft
		type = large_plane_air_transport_airframe_1
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "CASA/IPTN CN-235"
		amount = 1
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "YS-11M"
		amount = 3
		producer = JAP
	}
}