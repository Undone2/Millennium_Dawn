﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#F-5
		amount = 10
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#Mig-21
		amount = 11
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2		#Mig-29
		amount = 5
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1		#Su-22
		#version_name = "Su-22 Fitter"
		amount = 17
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_plane1		#An-26
		amount = 6
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_plane1		#C130
		amount = 3
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane2		#Il-76
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-8
		amount = 14
		producer = SOV
	}
}