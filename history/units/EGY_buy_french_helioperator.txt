units = {
	fleet = {
		name = "French Carrier"
		naval_base = 4076
		task_force = {
			name = "French Carrier"
			location = 4076
			ship = { name = "French Carrier" definition = carrier start_experience_factor = 0.65 equipment = { carrier_2 = { amount = 1 owner = FRA creator = FRA } } }
		}
	}
}