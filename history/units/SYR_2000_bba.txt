﻿instant_effect = {

	#Aircraft

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-23"
		amount = 134
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_1
		variant_name = "MiG-25 Foxbat"
		amount = 36
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-21s Bis"
		amount = 178
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		variant_name = "MiG-29 Fulcrum"
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Su-22M5 Fitter"
		amount = 90
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		variant_name = "Su-24M"
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Aero L-39"
		amount = 80
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 6
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Dassault Falcon 20"
		amount = 3
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Il-76"
		amount = 10
		producer = SOV
	}
}