﻿division_template = {
	name = "Brigade Mécanisée"

	division_names_group = BEL_MEC_01

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 2 y = 0 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		H_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Régiment Para-Commando"

	division_names_group = BEL_SPEC_01

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 1 y = 0 }
		L_Air_Inf_Bat = { x = 1 y = 1 }
	}

	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 1 }
		L_Engi_Comp = { x = 0 y = 2 }
	}
	priority = 2
}

units = {
	division = {
		#name = "1re Brigade Mécanisée"
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 9574		#Leopoldsburg
		division_template = "Brigade Mécanisée"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "7e Brigade Mécanisée"
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 11407		#Marche-en-Famenne
		division_template = "Brigade Mécanisée"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		#name = "Régiment Para-Commando"
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 11407		#Brussels
		division_template = "Régiment Para-Commando"
		start_experience_factor = 0.8
		start_equipment_factor = 0.01
	}

	#2003:
	# 2 mech bde
	# Régiment Para-Commando
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons2			#FN FNC
		amount = 4000
		producer = BEL
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#MILAN
		amount = 280
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1			#Mistral
		amount = 160
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 350
		producer = BEL
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 #HOT
		amount = 40
		producer = FRA
	}

	#Vehicles
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "Leopard 1A3"
		amount = 143
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "YPR-765"
		amount = 240
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M113"
		amount = 202
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "FV103"
		amount = 76
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 119
		producer = ENG
		variant_name = "FV107 Scimitar"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "Pandur"
		amount = 50
		producer = AUS
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "M109A2"
		amount = 112
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_2 #LG MK II
		amount = 13
		producer = FRA
	}
}