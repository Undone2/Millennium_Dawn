﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_2	#Su-25
		amount = 39
		producer = SOV
		variant_name = "Su-25"
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1		 #Mig-23
		amount = 30
		producer = SOV
		variant_name = "MiG-23"
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1		 #Mig-21
		amount = 81
		producer = SOV
		variant_name = "MiG-21s Bis"
	}

	add_equipment_to_stockpile = {
		type = small_plane_airframe_2 		#MiG-29
		amount = 21
		producer = SOV
		variant_name = "MiG-29 Fulcrum"
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1 	#Su-22
		amount = 21
		producer = SOV
		variant_name = "Su-22M5 Fitter"
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1 	#An-26
		amount = 7
		producer = SOV
		variant_name = "An-26"
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1 	#Let L-410
		amount = 6
		producer = CZE
		variant_name = "Let L-410"
	}
}