﻿division_template = {
	name = "Liwa Moudar'a"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		L_arm_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		attack_helo_comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Liwa Mushat Mechaniqui"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		attack_helo_comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Katiba Alquwwat Alkhasa"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	support = {
		attack_helo_comp = { x = 0 y = 0 }
	}

	priority = 2
}

units = {
	division = {
		name = "Liwa Moudar'a"
		location = 13208
		division_template = "Liwa Moudar'a"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	division = {
		name = "Liwa Mushat Mechaniqui"
		location = 13208
		division_template = "Liwa Mushat Mechaniqui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}

	division = {
		name = "Katiba Alquwwat Alkhasa"
		location = 13208
		division_template = "Katiba Alquwwat Alkhasa"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 200
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons			#G3
		amount = 500
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons			#FN FAL
		amount = 500
		producer = BEL
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#M16
		amount = 500
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "M60A3"
		amount = 106
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 22
		producer = FRA
		variant_name = "Panhard AML 90"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 8
		producer = ENG
		variant_name = "Alvis Saladin"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 8
		producer = ENG
		variant_name = "Ferret"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 8
		producer = ENG
		variant_name = "Shorland"
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "YPR-765"
		amount = 25
		producer = BEL
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "YPR-765"
		amount = 10
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M3 Panhard"
		amount = 110
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M113"
		amount = 220
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = artillery_1			#L118
		amount = 8
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = artillery_0			#M102
		#version_name = "M198 Howitzer"
		amount = 28
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "M110"
		amount = 62
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M270"
		amount = 9
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0			#TOW
		amount = 15
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1			#USA_Anti_Air_1:0 "FIM-92 Stinger" 18
		amount = 18
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1			#SWE_Anti_Air_1:0 "Robotsystem 70" #SWE_Anti_Air_1_short:0 "RBS 70" 40
		amount = 40
		producer = SWE
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 7
		producer = FRA
		variant_name = "Crotale"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 8
		producer = USA
		variant_name = "MIM-23 Hawk"
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		variant_name = "AH-1 Cobra"
		amount = 30
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AB 212
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #BO-105
		amount = 3
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #UH-60L + S-70a
		amount = 1
		producer = USA
	}
}