﻿division_template = {
	name = "Armoured Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		L_arm_Bat = { x = 0 y = 2 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Infantry Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}
}


division_template = {
	name = "Air Cavalry Battalion"

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
	}
	support = {
		attack_helo_comp = { x = 0 y = 0 }
		attack_helo_comp = { x = 0 y = 1 }
	}
}


division_template = {
	name = "Airborne Battalion"

	regiments = {
		Mot_Air_Inf_Bat = { x = 0 y = 0 }
	}
	support = {
		attack_helo_comp = { x = 0 y = 0 }
		attack_helo_comp = { x = 0 y = 1 }
	}
}

units = {
	division = {
		name = "1st Infantry Brigade"
		location = 1939
		division_template = "Infantry Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "Armoured Brigade"
		location = 1939
		division_template = "Armoured Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "2nd Infantry Brigade"
		location = 5210
		division_template = "Infantry Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}
	division = {
		name = "Air Cavalry Battalion"
		location = 5210
		division_template = "Air Cavalry Battalion"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
	division = {
		name = "Airborne Battalion"
		location = 5210
		division_template = "Airborne Battalion"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons		#G3
		amount = 600
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#AKM
		amount = 1600
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1		#Galil
		amount = 300
		producer = ISR
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons4		#M4 Carbine
		amount = 150
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons5		#FN SCAR
		amount = 150
		producer = BEL
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 300
		producer = ENG
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1			#MILAN
		amount = 100
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0			#TOW
		amount = 40
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1			#
		amount = 100
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "Vickers MBT Mk 3"
		amount = 76
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 72
		producer = FRA
		variant_name = "Panhard AML 90"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 12
		producer = ENG
		variant_name = "Ferret"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 8
		producer = ENG
		variant_name = "Shorland"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "UR-416"
		#version_name = "Henschel UR-416"
		amount = 52
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "M3 Panhard"
		amount = 10
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 300
		producer = KEN
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1		#SA-330 Puma
		amount = 3
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		variant_name = "MH-6M Little Bird"
		amount = 34
		producer = USA
	}
}