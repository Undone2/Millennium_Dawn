﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#L-39
		amount = 24
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #MiG-21
		amount = 50
		producer = SOV
	}
}