﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #"Nanchang J-6"
		amount = 6
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #Chengdu J-7
		amount = 6
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #Y-8
		amount = 2
		producer = CHI
	}
}