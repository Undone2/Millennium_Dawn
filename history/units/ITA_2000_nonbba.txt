﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = CV_L_Strike_fighter2 #AV-8B Harrier II
		amount = 18
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Tornado IDS
		amount = 104
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter3
		amount = 35
	}
	add_equipment_to_stockpile = {
		type = naval_plane3
		amount = 18
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #TF-104G
		amount = 18
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #Panavia Tornado ADV
		amount = 45
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #TF-104S
		amount = 205
	}
	add_equipment_to_stockpile = {
		type = cas2 #AMX International
		amount = 136
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Aermacchi MB-326
		amount = 106
		#version_name = "Aermacchi MB-326"
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #Aermacchi MB-339
		amount = 137
		#version_name = "Aermacchi MB-339"
	}
	add_equipment_to_stockpile = {
		type = transport_plane3 #C-130J Super Hercules
		amount = 31
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #Aeritalia G.222
		amount = 58
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #SIAI-Marchetti SF.260
		amount = 45
		#version_name = "SIAI-Marchetti SF.260"
	}
}