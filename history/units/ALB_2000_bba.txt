instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "J-6"
		amount = 78
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "J-7"
		amount = 10
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-5 Galaxy"
		amount = 10
		producer = USA
	}
}