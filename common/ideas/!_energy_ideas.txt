ideas = {
	hidden_ideas = {
		heavy_power_restrictions_spirit = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea some_power_restrictions_spirit" }
			picture = fusion_energy
			modifier = {
				production_speed_buildings_factor = -0.3
				political_power_factor = -0.15
				energy_use_multiplier = -0.2
				tax_gain_multiplier_modifier = -0.15
			}
		}
		some_power_restrictions_spirit = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea some_power_restrictions_spirit" }
			picture = fusion_energy
			modifier = {
				production_speed_buildings_factor = -0.1
				political_power_factor = -0.05
				energy_use_multiplier = -0.1
				tax_gain_multiplier_modifier = -0.05
			}
		}
		some_additional_consumption_spirit = { # values halved except for energy use on 11/26/23
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea some_additional_consumption_spirit" }
			picture = fusion_energy
			modifier = {
				production_speed_buildings_factor = 0.05
				political_power_factor = 0.025
				energy_use_multiplier = 0.1
				tax_gain_multiplier_modifier = 0.025
			}
		}
		heavy_additional_consumption_spirit = {# values halved except for energy use on 11/26/23
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea some_additional_consumption_spirit" }
			picture = fusion_energy
			modifier = {
				production_speed_buildings_factor = 0.075
				political_power_factor = 0.037
				energy_use_multiplier = 0.2
				tax_gain_multiplier_modifier = 0.037
			}
		}
	}
}
