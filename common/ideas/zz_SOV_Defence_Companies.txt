ideas = {
	tank_manufacturer = {
		designer = yes
		SOV_military_industry_company_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_military_industry_company_tank_manufacturer" }
			picture = Military_Industry_Company_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARTILLERY = 0.155
			}

			traits = {
				Cat_ARTILLERY_5

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_rostec_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_tank_manufacturer" }
			picture = Rostec_SOV
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_ARMOR = 0.248
			}
			traits = {
				Cat_ARMOR_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_kurganmashzavod_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_kurganmashzavod_tank_manufacturer" }
			picture = Kurganmashzavod_SOV
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_AFV = 0.217
			}

			traits = {
				Cat_AFV_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		SOV_rostec_materiel_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_materiel_manufacturer" }
			picture = Rostec_SOV
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF = 0.248
			}

			traits = {
				Cat_INF_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_almaz_antey_materiel_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_almaz_antey_materiel_manufacturer" }

			picture = Almaz_Antey_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF_WEP = 0.248
			}

			traits = {
				Cat_INF_WEP_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_jsc_defense_systems_materiel_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_jsc_defense_systems_materiel_manufacturer" }
			picture = JSC_Defense_systems_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_AA = 0.186
			}

			traits = {
				Cat_AA_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes
		SOV_russian_helicopters_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_russian_helicopters_tank_manufacturer" }
			picture = Russian_Helicopters_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.186
			}

			traits = {
				Cat_HELI_6

			}
			ai_will_do = {
				factor = 1
			}
		}
		SOV_rostec_tank_manufacturer2 = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_rostec_tank_manufacturer" }

			picture = Rostec_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = {
				Cat_HELI_8

			}
			ai_will_do = {
				factor = 1
			}
		}

		SOV_mil_helicopters_tank_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_mil_helicopters_tank_manufacturer" }

			picture = Mil_Helicopters_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = {
				Cat_HELI_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		SOV_united_aircraft_corporation_aircraft_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_aircraft_corporation_aircraft_manufacturer" }

			picture = United_Aircraft_Corporation_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_FIXED_WING = 0.248
			}

			traits = {
				CAT_FIXED_WING_8

			}
			ai_will_do = {
				factor = 1
			}
		}

		SOV_irkut_aircraft_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_irkut_aircraft_manufacturer" }

			picture = Irkut_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_FIGHTER = 0.216
			}

			traits = {
				Cat_FIGHTER_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}


	naval_manufacturer = {

		designer = yes

		SOV_united_shipbuilding_corporation_naval_manufacturer = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_shipbuilding_corporation_naval_manufacturer" }

			picture = United_Shipbuilding_Corporation_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}

			traits = {
				Cat_NAVAL_EQP_6

			}
			ai_will_do = {
				factor = 1
			}
		}

		SOV_united_shipbuilding_corporation_naval_manufacturer2 = {
			allowed = { original_tag = SOV }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_united_shipbuilding_corporation_naval_manufacturer" }

			picture = United_Shipbuilding_Corporation_SOV
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_SUB = 0.186
			}

			traits = {
				Cat_SUB_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}