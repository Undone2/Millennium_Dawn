ideas = {

	materiel_manufacturer = {

		designer = yes

		ITA_beretta_materiel_manufacturer = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_beretta_materiel_manufacturer" }

			picture = Beretta_ITA
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF_WEP = 0.186
			}

			traits = {
				Cat_INF_WEP_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		ITA_iveco_tank_manufacturer = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_iveco_tank_manufacturer" }

			picture = Iveco_ITA

			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARMOR = 0.186
			}

			traits = {
				Cat_ARMOR_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		ITA_agustawestland_tank_manufacturer = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_agustawestland_tank_manufacturer" }

			picture = AgustaWestland_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = {
				Cat_HELI_8
			}
			ai_will_do = {
				factor = 1
			}
		}

		ITA_airbus_helicopters_tank_manufacturer = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_airbus_helicopters_tank_manufacturer" }
			picture = Airbus_Helicopters_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = { Cat_soft_HELI_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		ITA_fincantieri_naval_manufacturer = {
			allowed = {
				original_tag = ITA
			}
			visible = {
				NOT = {
					has_completed_focus = ITA_focus_fincantieri_submarines
					has_completed_focus = ITA_focus_fincantieri_surface
				}
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_naval_manufacturer" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.248
			}

			traits = { Cat_NAVAL_EQP_8 }
			ai_will_do = {
				factor = 1
			}
		}

		ITA_fincantieri_naval_manufacturer_unfocused = {
			allowed = {
				original_tag = ITA
			}
			visible = {
				has_completed_focus = ITA_focus_fincantieri_submarines
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_naval_manufacturer_unfocused" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}

			traits = { Cat_NAVAL_EQP_6 }
			ai_will_do = {
				factor = 1
			}
		}

		ITA_fincantieri_naval_manufacturer_improved = {
			allowed = {
				original_tag = ITA
			}
			visible = {
				has_completed_focus = ITA_focus_fincantieri_surface
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_naval_manufacturer_improved" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.3
			}

			traits = { Cat_NAVAL_EQP_10 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		ITA_fincantieri_naval_manufacturer2 = {
			allowed = {
				original_tag = ITA
			}
			visible = {
				NOT = {
					has_completed_focus = ITA_focus_fincantieri_submarines
					has_completed_focus = ITA_focus_fincantieri_surface
				}
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_naval_manufacturer" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.248
			}

			traits = { Cat_D_SUB_8 }
			ai_will_do = {
				factor = 1
			}
		}

		ITA_fincantieri_naval_manufacturer_unfocused2 = {
			allowed = {
				original_tag = ITA
			}
			visible = {
				has_completed_focus = ITA_focus_fincantieri_surface
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_naval_manufacturer_unfocused" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.186
			}

			traits = { Cat_D_SUB_6 }
			ai_will_do = {
				factor = 1
			}
		}

		ITA_fincantieri_naval_manufacturer_improved2 = {
			allowed = {
				original_tag = ITA
			}
			visible = {
				has_completed_focus = ITA_focus_fincantieri_submarines
			}
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_naval_manufacturer" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.3
			}

			traits = { Cat_D_SUB_10 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		ITA_alenia_aeronautica_aircraft_manufacturer = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_alenia_aeronautica_aircraft_manufacturer" }
			picture = Alenia_Aeronautica_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_FIXED_WING = 0.217
			}

			traits = {
				Cat_L_Fighter_7

			}
			ai_will_do = {
				factor = 1
			}
		}
		ITA_airbus_defence_aircraft_manufacturer = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_airbus_defence_aircraft_manufacturer" }
			picture = Airbus_Defence_ITA
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_FIXED_WING = 0.279
			}

			traits = {
				CAT_FIXED_WING_9

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}