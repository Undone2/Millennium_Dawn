ideas = {
	materiel_manufacturer = {
		designer = yes
		NKO_1_bureau_materiel_manufacturer = {
			allowed = { original_tag = NKO }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NKO_1_bureau_materiel_manufacturer" }
			picture = NK_Arms
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF = 0.248
			}

			traits = { Cat_INF_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		NKO_6_bureau_naval_manufacturer = {
			allowed = { original_tag = NKO }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NKO_6_bureau_naval_manufacturer" }

			picture = NK_Navy
			cost = 150
			removal_cost = 10

			research_bonus = {
				Cat_D_SUB = 0.279
			}

			traits = {
				Cat_D_SUB_9
			}
			ai_will_do = {
				factor = 1
			}
		}

	}
	tank_manufacturer = {
		designer = yes
		NKO_2_bureau_tank_manufacturer = {
			allowed = { original_tag = NKO }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NKO_2_bureau_tank_manufacturer" }
			picture = NK_Tanks
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_ARMOR = 0.248
			}

			traits = { Cat_ARMOR_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}
	naval_manufacturer = {

		designer = yes

		NKO_6_bureau_naval_manufacturer2 = {
			allowed = { original_tag = NKO }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea NKO_6_bureau_naval_manufacturer" }

			picture = NK_Navy

			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_NAVAL_EQP = 0.217
			}

			traits = {
				Cat_NAVAL_EQP_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
