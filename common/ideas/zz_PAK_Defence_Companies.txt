ideas = {
	tank_manufacturer = {
		designer = yes
		PAK_heavy_industries_taxila_tank_manufacturer = {
			allowed = { original_tag = PAK }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PAK_heavy_industries_taxila_tank_manufacturer" }
			picture = Heavy_Industries_Taxila
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_ARMOR = 0.186
			}

			traits = { Cat_ARMOR_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		PAK_pakistan_aeronautical_complex_aircraft_manufacturer = {
			allowed = { original_tag = PAK }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PAK_pakistan_aeronautical_complex_aircraft_manufacturer" }
			picture = Pakistan_aeronautical
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_FIGHTER = 0.124
			}

			traits = { Cat_FIGHTER_4 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {
		designer = yes
		PAK_integrated_defence_systems_materiel_manufacturer = {
			allowed = { original_tag = PAK }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PAK_integrated_defence_systems_materiel_manufacturer" }
			picture = Integraded_Dynamics
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF_WEP = 0.155
			}

			traits = { Cat_INF_WEP_5 }
			ai_will_do = {
				factor = 1
			}
		}

		PAK_pakistan_ordnance_factories_materiel_manufacturer = {
			allowed = { original_tag = PAK }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PAK_pakistan_ordnance_factories_materiel_manufacturer" }
			picture = Pakistan_Ordnance
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF_WEP = 0.155
			}

			traits = { Cat_INF_WEP_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		PAK_karachi_shipyard_naval_manufacturer = {
			allowed = { original_tag = PAK }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PAK_karachi_shipyard_naval_manufacturer" }
			picture = Karachi
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.155
			}

			traits = { Cat_NAVAL_EQP_5 }
			ai_will_do = {
				factor = 1
			}
		}

		PAK_karachi_shipyard_naval_manufacturer2 = {
			allowed = { original_tag = PAK }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PAK_karachi_shipyard_naval_manufacturer" }
			picture = Karachi
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.155
			}

			traits = { Cat_D_SUB_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
