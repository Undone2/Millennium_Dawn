ideas = {
	materiel_manufacturer = {
		designer = yes
		RAJ_ordnance_factories_organization_materiel_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_ordnance_factories_organization_materiel_manufacturer" }
			picture = Ordnance_Factories_Organization_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF_WEP = 0.186
			}

			traits = { Cat_INF_WEP_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes
		RAJ_ordnance_factories_organization_tank_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_ordnance_factories_organization_tank_manufacturer" }
			picture = Ordnance_Factories_Organization_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_ARMOR = 0.186
			}

			traits = { Cat_ARMOR_6 }
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_tata_motors_tank_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_tata_motors_tank_manufacturer" }
			picture = Tata_Motors_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_AFV = 0.186
			}

			traits = { Cat_AFV_6 }
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_hindustan_aeronautics_tank_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_hindustan_aeronautics_tank_manufacturer" }
			picture = Hindustan_Aeronautics_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_HELI = 0.217
			}

			traits = { Cat_HELI_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		RAJ_hindustan_aeronautics_aircraft_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_hindustan_aeronautics_aircraft_manufacturer" }
			picture = Hindustan_Aeronautics_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_FIXED_WING = 0.217
			}

			traits = { CAT_FIXED_WING_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		RAJ_cochin_shipyard_naval_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_cochin_shipyard_naval_manufacturer" }
			picture = Cochin_Shipyard_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_CARRIER = 0.156
			}

			traits = { Cat_CARRIER_5 }
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_mazagon_dock_limited_naval_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_mazagon_dock_limited_naval_manufacturer" }
			picture = Mazagon_Dock_Limited_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.217
			}

			traits = { Cat_NAVAL_EQP_7 }
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_goa_shipyard_naval_manufacturer = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_goa_shipyard_naval_manufacturer" }
			picture = Goa_Shipyard_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_TRANS_SHIP = 0.217
			}

			traits = { Cat_TRANS_SHIP_7 }
			ai_will_do = {
				factor = 1
			}
		}

		RAJ_mazagon_dock_limited_naval_manufacturer2 = {
			allowed = { original_tag = RAJ }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea RAJ_mazagon_dock_limited_naval_manufacturer" }
			picture = Mazagon_Dock_Limited_RAJ
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.217
			}

			traits = { Cat_D_SUB_7 }
			ai_will_do = {
				factor = 1
			}
		}

	}
}
