ideas = {
	materiel_manufacturer = {
		designer = yes
		USA_alliant_techsystems_materiel_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_alliant_techsystems_materiel_manufacturer" }
			picture = Alliant_Techsystems
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_AT = 0.217
			}

			traits = { Cat_AT_7 }
			ai_will_do = {
				factor = 1
			}
		}
		USA_textron_tank_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_textron_tank_manufacturer" }
			picture = Bell_Textron
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF = 0.279
			}

			traits = { Cat_INF_WEP_9 }
			ai_will_do = {
				factor = 1
			}
		}
		USA_colt_defense_materiel_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_colt_defense_materiel_manufacturer" }
			picture = Colt-Defense
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF_WEP = 0.217
			}

			traits = { Cat_INF_WEP_7 }
			ai_will_do = {
				factor = 1
			}
		}
		USA_consolidated_robotics_materiel_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_consolidated_robotics_materiel_manufacturer" }
			picture = united_defense
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF = 0.279
			}

			traits = { Cat_INF_WEP_9 }
			ai_will_do = {
				factor = 1
			}
		}
		USA_raytheon_materiel_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_raytheon_materiel_manufacturer" }

			picture = Raytheon
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_AA = 0.248
			}

			traits = {
				Cat_AA_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		USA_bell_helicopter_textron_tank_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_bell_helicopter_textron_tank_manufacturer" }

			picture = textron
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = {
				Cat_HELI_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		USA_sikorsky_aircraft_tank_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_sikorsky_aircraft_tank_manufacturer" }

			picture = Sikorsky_Aircraft_Logo

			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.217
			}

			traits = {
				Cat_HELI_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		USA_boeing_aircraft_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_boeing_aircraft_manufacturer" }

			picture = Boeing
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_FIXED_WING = 0.279
			}

			traits = {
				CAT_FIXED_WING_9

			}
			ai_will_do = {
				factor = 1
			}
		}

		USA_lockheed_martin_aircraft_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_lockheed_martin_aircraft_manufacturer" }

			picture = Lockheed_Martin
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_FIXED_WING = 0.279
			}

			traits = {
				Cat_FIGHTER_9

			}
			ai_will_do = {
				factor = 1
			}
		}

		USA_northrop_grumman_aircraft_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_northrop_grumman_aircraft_manufacturer" }
			picture = Northrop_Grumman
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_H_AIR = 0.248
			}

			traits = {
				Cat_H_AIR_8

			}
			ai_will_do = {
				factor = 1
			}
		}
	}


	naval_manufacturer = {

		designer = yes

		USA_general_dynamics_electric_boat_naval_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_general_dynamics_electric_boat_naval_manufacturer" }

			picture = General_Dynamics_Electric_Boat
			cost = 150
			removal_cost = 10

			research_bonus = {
				Cat_D_SUB = 0.279
			}

			traits = {
				Cat_D_SUB_9

			}
			ai_will_do = {
				factor = 1
			}
		}

		USA_northrop_grumman_newport_news_naval_manufacturer = {
			allowed = { original_tag = USA  }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_northrop_grumman_newport_news_naval_manufacturer" }
			picture = NNS
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_NUKE_SUB = 0.279
			}

			traits = {
				Cat_NUKE_SUB_9

			}
			ai_will_do = {
				factor = 1
			}
		}

		USA_bath_iron_works_naval_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_bath_iron_works_naval_manufacturer" }

			picture = Bath_Iron_Works
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_NAVAL_EQP = 0.279
			}

			traits = { Cat_NAVAL_EQP_9 }
			ai_will_do = {
				factor = 1
			}
		}

		USA_national_steel_and_shipbuilding_company_naval_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_national_steel_and_shipbuilding_company_naval_manufacturer" }
			picture = General_Dynamics
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_TRANS_SHIP = 0.186
			}

			traits = {
				Cat_TRANS_SHIP_6

			}
			ai_will_do = {
				factor = 1
			}
		}

		USA_ingalls_shipbuilding_naval_manufacturer = {
			allowed = { original_tag = USA  }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_ingalls_shipbuilding_naval_manufacturer" }

			picture = ingalls-logo
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_SURFACE_SHIP = 0.248
			}

			traits = {
				Cat_SURFACE_SHIP_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		USA_northrop_grumman_newport_news_naval_manufacturer2 = {
			allowed = { original_tag = USA  }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_northrop_grumman_newport_news_naval_manufacturer" }
			picture = NNS
			cost = 150
			removal_cost = 10

			research_bonus = {
				Cat_CARRIER = 0.279
			}

			traits = {
				Cat_CARRIER_9

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

		tank_manufacturer = {
		designer = yes
		USA_united_defense_tank_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_united_defense_tank_manufacturer" }
			picture = united_defense
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARTILLERY = 0.248
			}

			traits = {
				Cat_AFV_8

			}
			ai_will_do = {
				factor = 1
			}
		}
		USA_general_dynamics_materiel_manufacturer = {
			allowed = { original_tag = USA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea USA_general_dynamics_materiel_manufacturer" }
			picture = General_Dynamics
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_AFV = 0.248
			}

			traits = { CAT_TANKS_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}
