UZB = {
	air_wing_names_template = AIR_WING_NAME_UZB_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC
	}
	small_plane_strike_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	small_plane_cas_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Uchuvchisiz qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	medium_plane_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC
		unique = {
			"3 Qiruvchi samolyot" "7 Qiruvchi samolyot" "8 Qiruvchi samolyot"
		}
	}
	medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	medium_plane_suicide_airframe = {
		prefix = ""
		generic = { "Uchuvchisiz qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	cv_medium_plane_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC
	}
	cv_medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	cv_medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	cv_medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	cv_medium_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transport samolyoti" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	large_plane_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	large_plane_cas_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	large_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	large_plane_awacs_airframe = {
		prefix = ""
		generic = { "Qiruvchi samolyot" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	large_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transport samolyoti" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}
	attack_helicopter_equipment = {
		prefix = ""
		generic = { "Hujum vertolyoti" }
		generic_pattern = AIR_WING_NAME_UZB_GENERIC

	}

}