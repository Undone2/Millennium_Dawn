AST = {
	air_wing_names_template = AIR_WING_NAME_AST_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	small_plane_suicide_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	small_plane_strike_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	small_plane_cas_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	small_plane_naval_bomber_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_small_plane_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_small_plane_suicide_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_small_plane_strike_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_small_plane_cas_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_small_plane_naval_bomber_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	medium_plane_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	medium_plane_fighter_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC	}
	medium_plane_maritime_patrol_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC	}
	medium_plane_cas_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC	}
	medium_plane_suicide_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_medium_plane_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_medium_plane_fighter_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_medium_plane_cas_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_medium_plane_maritime_patrol_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_medium_plane_air_transport_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	cv_medium_plane_scout_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	large_plane_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	large_plane_cas_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	large_plane_awacs_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	large_plane_maritime_patrol_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	large_plane_air_transport_airframe = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	attack_helicopter_hull = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
	guided_missile_equipment = {
		prefix = "No. "
		generic = { "Wing RAAF" }
		generic_pattern = AIR_WING_NAME_AST_GENERIC
	}
}
