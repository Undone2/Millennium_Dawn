﻿### TRANSLATIONS ###
# Zerehi - Armored
# Motorizeh - Motorized
# Lashkar - Division/Soldier
# Tip-e - Brigade
# Komandoyee - Commando
# Mechanize - Mechanized
# Tofangdarane Daryayee - Marines
# Chatrbaz - Parachuters



PER_ARM_01 = {
	name = "Armored Brigade"

	for_countries = { PER }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Tip-e Zerehi"

	ordered = {
		1 = { "181st Tip-e Zerehi" }
		2 = { "281st Tip-e Zerehi" }
		3 = { "381st Tip-e Zerehi" }
		4 = { "188th Tip-e Zerehi" }
		5 = { "288th Tip-e Zerehi" }
		6 = { "388th Tip-e Zerehi" }
		7 = { "216th Tip-e Zerehi" }
		8 = { "316th Tip-e Zerehi" }
	}
}
#
PER_ARM_02 = {
	name = "Armored Division"

	for_countries = { PER }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Lashkar-e Zerehi"

	ordered = {
		1 = { "190th Lashkar-e Zerehi" }
		2 = { "290th Lashkar-e 'Eghtedar'" }
		3 = { "390th Lashkar-e 'Hath'" }
		4 = { "141st Lashkar-e 'Shiran'" }
		5 = { "241st Lashkar-e 'Aqrab'" }
		7 = { "341st Lashkar-e 'Amir'" }
		8 = { "165th Lashkar-e 'Noor'" }
		9 = { "265th Lashkar-e 'Fazlollah'" }
		10 = { "365th Lashkar-e 'Sayyad'" }
	}
}
# #
PER_MEC_01 = {
	name = "Mechanized Brigade"

	for_countries = { PER }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Tip-e Mechanize"

	ordered = {
		1 = { "77th Tip-e Mechanize" }
		2 = { "84th Tip-e Mechanize" }
		3 = { "181st Tip-e Mechanize" }
		4 = { "281st Tip-e Mechanize" }
		5 = { "381st Tip-e Mechanize" }
		6 = { "188th Tip-e Mechanize" }
		7 = { "288th Tip-e Mechanize" }
		8 = { "388th Tip-e Mechanize" }
		9 = { "216th Tip-e Mechanize" }
		10 = { "316th Tip-e Mechanize" }
	}
}
#
PER_MEC_02 = {
	name = "Mechanized Division"

	for_countries = { PER }



	division_types = { "armor_Bat" }

	fallback_name = "%d. Lashkar-e Mechanize"

	ordered = {
		1 = { "258th Lashkar-e Mechanize" }
		2 = { "358th Lashkar-e Mechanize" }
		3 = { "458th Lashkar-e Mechanize" }
		4 = { "241st Lashkar-e Mechanize" }
		5 = { "341st Lashkar-e Mechanize" }
		6 = { "143rd Lashkar-e Mechanize" }
		7 = { "243rd Lashkar-e Mechanize" }
		8 = { "343rd Lashkar-e Mechanize" }
	}
}
#
PER_INF_01 = {
	name = "Infantry Brigade"

	for_countries = { PER }

	division_types = { "Mot_Inf_Bat" }

	fallback_name = "%d. Tip-e Piade Nezam"

	ordered = {
		1 = { "221st Tip-e Piade Nezam" }
		2 = { "321st Tip-e Piade Nezam" }
		3 = { "421st Tip-e Piade Nezam" }
		4 = { "128th Tip-e Piade Nezam" }
		5 = { "228th Tip-e Piade Nezam" }
		6 = { "328th Tip-e Piade Nezam" }
		7 = { "130th Tip-e Piade Nezam" }
		8 = { "230th Tip-e Piade Nezam" }
		9 = { "330th Tip-e Piade Nezam" }
		10 = { "164th Tip-e Piade Nezam" }
		11 = { "264th Tip-e Piade Nezam" }
		12 = { "364th Tip-e Piade Nezam" }
	}
}
#
PER_INF_02 = {
	name = "Infantry Division"

	for_countries = { PER }

	division_types = { "Mot_Inf_Bat" }

	fallback_name = "%d. Lashkar-e Motorizeh"

	ordered = {
		1 = { "54th Lashkar-e Motorizeh" }
		2 = { "55th Lashkar-e Motorizeh" }
		3 = { "56th Lashkar-e Motorizeh" }
		4 = { "57th Lashkar-e Motorizeh" }
		5 = { "155th Lashkar-e Motorizeh" }
		6 = { "255th Lashkar-e Motorizeh" }
		7 = { "355th Lashkar-e Motorizeh" }
	}
}
#
PER_COM_01 = {
	name = "Commando Division"

	for_countries = { PER }

	division_types = { "L_Air_assault_Bat" }

	fallback_name = "%d. Lashkar-e Komandoyee"

	ordered = {
		1 = { "212th Lashkar-e Komandoyee" }
		2 = { "312th Lashkar-e Komandoyee" }
		3 = { "412th Lashkar-e Komandoyee" }
		4 = { "512th Lashkar-e Komandoyee" }
		5 = { "115th Lashkar-e Komandoyee" }
		6 = { "215th Lashkar-e Komandoyee" }
		7 = { "315th Lashkar-e Komandoyee" }
		8 = { "44th Lashkar-e Komandoyee" }
		9 = { "45th Lashkar-e Komandoyee" }
		10 = { "46th Lashkar-e Komandoyee" }
	}
}
#
PER_COM_02 = {
	name = "Commando Brigade"

	for_countries = { PER }

	division_types = { "Special_Forces" }

	fallback_name = "%d. Tip-e Komandoyee"

	ordered = {
		1 = { "113st Tip-e Komandoyee" }
		2 = { "313st Tip-e Komandoyee" }
		3 = { "413st Tip-e Komandoyee" }
		4 = { "71st Tip-e Komandoyee" }
		5 = { "81st Tip-e Komandoyee" }
		6 = { "91st Tip-e Komandoyee" }
		7 = { "781st Tip-e Komandoyee" }
		8 = { "881st Tip-e Komandoyee" }
		9 = { "981st Tip-e Komandoyee" }
	}
}
#
PER_AIR_01 = {
	name = "Airborne Brigade"

	for_countries = { PER }

	division_types = { "L_Air_assault_Bat" }

	fallback_name = "%d. Tip-e Chatrbaz"

	ordered = {
		1 = { "321st Tip-e Chatrbaz" }
		2 = { "421st Tip-e 'Simurgh'" }
		3 = { "165th Tip-e 'Parandeh'" }
		4 = { "265th Tip-e 'Tooti'" }
		5 = { "365th Tip-e 'Parvaz'" }
	}
}
#
PER_MAR_01 = {
	name = "Marine Brigade"

	for_countries = { PER }

	division_types = { "L_Marine_Bat" }

	fallback_name = "%d. Tofangdarane Daryayee"

	ordered = {
		1 = { "21st Tofangdarane Daryayee" }
		2 = { "31st Tofangdarane Daryayee" }
		3 = { "41st Tofangdarane Daryayee" }
		4 = { "147th Tofangdarane Daryayee" }
		5 = { "247th Tofangdarane Daryayee" }
		6 = { "347th Tofangdarane Daryayee" }
		7 = { "447th Tofangdarane Daryayee" }
		8 = { "92nd Tofangdarane Daryayee" }
		9 = { "192nd Tofangdarane Daryayee" }
		10 = { "292nd Tofangdarane Daryayee" }
	}
}
