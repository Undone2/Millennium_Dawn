opinion_modifiers = {

	SIN_released_alan_shadrake = {
		value = 20
		decay = -11
	}

	SIN_heavy_punishment_alan_shadrake = {
		value = -50
		decay = -1
	}

	SIN_trial_by_jury_shadrake = {
		value = -25
		decay = -1
	}

	SIN_provided_clemency_to_african_men = {
		value = 15
		decay = -1
	}

	SIN_executed_african_men = {
		value = -15
		decay = -11
	}

	SIN_extradited_to_bangladesh = {
		value = 20
		decay = -1
	}

	SIN_anti_f1_sentiment = {
		value = -20
		decay = -1
	}

	SIN_pro_f1_sentiment = {
		value = 20
		decay = -1
	}
}

