on_actions = {
	on_monthly_PAK = {
		effect = {
			random_list = {
				50 = {
					subtract_from_variable = { PAK.kasmir_readiness = 5 }
					clamp_variable = {
						var = PAK.kasmir_readiness
						min = 0
						max = 100
					}
				}
				50 = {
					# nothing
				}
			}
			# tension is handled in RAJ
		}
	}
}