is_neo_baathist_uar = {
	custom_trigger_tooltip = {
		tooltip = is_neo_baathist_uar_tt
		OR = {
			has_country_flag = formed_neo_baathist_uar
			has_country_flag = united_neo_baathist_uar
		}
	}
}

is_baathist_uar = {
	custom_trigger_tooltip = {
		tooltip = is_baathist_uar_tt
		OR = {
			has_country_flag = formed_baathist_uar
			has_country_flag = united_baathist_uar
		}
	}
}