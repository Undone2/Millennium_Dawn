VEN_mission_vuelta_al_campo_modifier = {
	icon = "GFX_idea_agricultural_reforms"

	enable = { always = yes }

	state_production_speed_buildings_factor = 0.15
	state_productivity_growth_modifier = 0.25
}
