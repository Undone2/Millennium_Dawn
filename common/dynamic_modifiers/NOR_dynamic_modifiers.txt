# RENEWABLE ENERGY
NOR_wind_farms = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	state_renewable_energy_generation_modifier = 0.40
	local_building_slots_factor = -0.05
}
NOR_offshore_wind_farms = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	state_renewable_energy_generation_modifier = 0.50
}
NOR_solar_farms = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	state_renewable_energy_generation_modifier = 0.25
	local_building_slots_factor = -0.05
}
NOR_solar_panels = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	state_renewable_energy_generation_modifier = 0.15
}
NOR_solar_panels = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	state_renewable_energy_generation_modifier = 0.15
}
NOR_bergen_line = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	local_supplies = 0.15
}
NOR_dovre_line = {
	icon = GFX_idea_sustainable_development
	enable = { always = yes }

	local_supply_impact_factor = -0.25
}