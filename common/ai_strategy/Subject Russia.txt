# Written by LordBogdanoff

#Support Sirya Like Russia
Subject_siryan_campaign = {
	allowed = {
		OR = {
			original_tag = CHE
			original_tag = YAK
			original_tag = PMR
			original_tag = DAG
			original_tag = CRM
			original_tag = KAE
			original_tag = TAT
			original_tag = BSH
			original_tag = NEE
			original_tag = YAM
			original_tag = LPR
			original_tag = DPR
			original_tag = CKK
			original_tag = KHS
			original_tag = ALT
			original_tag = URA
			original_tag = SIB
			original_tag = FAR
			original_tag = GOR
			original_tag = KUB
			original_tag = TUV
			original_tag = KOM
			original_tag = ADY
			original_tag = KBK
			original_tag = ING
			original_tag = BRY
			original_tag = KCC
			original_tag = SOO
			original_tag = KLM
			original_tag = KHM
			original_tag = MLR
			original_tag = CHU
			original_tag = MEL
			original_tag = MOV
			original_tag = UDM
		}
	}
	enable = {
		SYR = { NOT = { has_war_with = SOV  } }
		has_autonomy_state = autonomy_republic_rf 
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = send_volunteers_desire id = "SYR" value = 1000 }
	ai_strategy = { type = befriend id = "SYR" value = 250 }
	ai_strategy = { type = support id = "SYR" value = 550 }
}
#Support Lugansk Like Russia
Subject_Lugansk_campaign = {
	allowed = {
		OR = {
			original_tag = CHE
			original_tag = YAK
			original_tag = PMR
			original_tag = DAG
			original_tag = CRM
			original_tag = KAE
			original_tag = TAT
			original_tag = BSH
			original_tag = NEE
			original_tag = YAM
			original_tag = LPR
			original_tag = DPR
			original_tag = CKK
			original_tag = KHS
			original_tag = ALT
			original_tag = URA
			original_tag = SIB
			original_tag = FAR
			original_tag = GOR
			original_tag = KUB
			original_tag = TUV
			original_tag = KOM
			original_tag = ADY
			original_tag = KBK
			original_tag = ING
			original_tag = BRY
			original_tag = KCC
			original_tag = SOO
			original_tag = KLM
			original_tag = KHM
			original_tag = MLR
			original_tag = CHU
			original_tag = MEL
			original_tag = MOV
			original_tag = UDM
		}
	}
	enable = {
		LPR = { NOT = { has_war_with = SOV } }
		has_autonomy_state = autonomy_republic_rf 
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = send_volunteers_desire id = "LPR" value = 1000 }
	ai_strategy = { type = befriend id = "LPR" value = 250 }
	ai_strategy = { type = support id = "LPR" value = 550 }
}
#Support Donetsk Like Russia
Subject_Donetsk_campaign = {
	allowed = {
		OR = {
			original_tag = CHE
			original_tag = YAK
			original_tag = PMR
			original_tag = DAG
			original_tag = CRM
			original_tag = KAE
			original_tag = TAT
			original_tag = BSH
			original_tag = NEE
			original_tag = YAM
			original_tag = LPR
			original_tag = DPR
			original_tag = CKK
			original_tag = KHS
			original_tag = ALT
			original_tag = URA
			original_tag = SIB
			original_tag = FAR
			original_tag = GOR
			original_tag = KUB
			original_tag = TUV
			original_tag = KOM
			original_tag = ADY
			original_tag = KBK
			original_tag = ING
			original_tag = BRY
			original_tag = KCC
			original_tag = SOO
			original_tag = KLM
			original_tag = KHM
			original_tag = MLR
			original_tag = CHU
			original_tag = MEL
			original_tag = MOV
			original_tag = UDM
		}
	}
	enable = {
		LPR = { NOT = {  has_war_with = SOV } }
		has_autonomy_state = autonomy_republic_rf 
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "DPR" value = 1000 }
	ai_strategy = { type = befriend id = "DPR" value = 250 }
	ai_strategy = { type = support id = "DPR" value = 550 }
}
#Support Kharkiv Like Russia
Subject_Kharkiv_campaign = {
	allowed = {
		OR = {
			original_tag = CHE
			original_tag = YAK
			original_tag = PMR
			original_tag = DAG
			original_tag = CRM
			original_tag = KAE
			original_tag = TAT
			original_tag = BSH
			original_tag = NEE
			original_tag = YAM
			original_tag = LPR
			original_tag = DPR
			original_tag = CKK
			original_tag = KHS
			original_tag = ALT
			original_tag = URA
			original_tag = SIB
			original_tag = FAR
			original_tag = GOR
			original_tag = KUB
			original_tag = TUV
			original_tag = KOM
			original_tag = ADY
			original_tag = KBK
			original_tag = ING
			original_tag = BRY
			original_tag = KCC
			original_tag = SOO
			original_tag = KLM
			original_tag = KHM
			original_tag = MLR
			original_tag = CHU
			original_tag = MEL
			original_tag = MOV
			original_tag = UDM
		}
	}
	enable = {
		HPR = { NOT = {  has_war_with = SOV } }
		has_autonomy_state = autonomy_republic_rf 
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "HPR" value = 1000 }
	ai_strategy = { type = befriend id = "HPR" value = 250 }
	ai_strategy = { type = support id = "HPR" value = 550 }
}