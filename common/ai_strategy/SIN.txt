SIN_befriend_asean = {
	allowed = { original_tag = SIN }
	enable = {
		NOT = { has_government = nationalist }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "VIE" value = 100 }
	ai_strategy = { type = befriend id = "LAO" value = 100 }
	ai_strategy = { type = befriend id = "CBD" value = 100 }
	ai_strategy = { type = befriend id = "IND" value = 100 }
	ai_strategy = { type = befriend id = "BRU" value = 100 }
	ai_strategy = { type = befriend id = "PHI" value = 100 }
	ai_strategy = { type = befriend id = "SIA" value = 100 }
	ai_strategy = { type = befriend id = "BRM" value = 100 }
	ai_strategy = { type = befriend id = "MAY" value = 100 }
}

SIN_befriend_asian_nations = {
	allowed = { original_tag = SIN }
	enable = {
		NOT = { has_government = nationalist }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "JAP" value = 100 }
	ai_strategy = { type = befriend id = "TAI" value = 100 }
	ai_strategy = { type = befriend id = "HKG" value = 100 }
	ai_strategy = { type = befriend id = "CHI" value = 100 }
	ai_strategy = { type = befriend id = "RAJ" value = 100 }
	ai_strategy = { type = befriend id = "BAN" value = 100 }
	ai_strategy = { type = befriend id = "BHU" value = 100 }
	ai_strategy = { type = befriend id = "NEP" value = 100 }
	ai_strategy = { type = befriend id = "NZL" value = 100 }
	ai_strategy = { type = befriend id = "AST" value = 100 }
}

SIN_american_singaporean_relations = {
	allowed = { original_tag = SIN }
	enable = {
		NOT = { has_government = nationalist }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "USA" value = 100 }
}