MAY_singapore_is_expansionary = {
	allowed = { original_tag = MAY }
	enable = {
		SIN = {
			OR = {
				has_completed_focus = SIN_expansionary_military_policy
				has_completed_focus = SIN_pressure_the_south_asian_seas
			}
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = {
		type = contain
		id = "SIN"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "SIN"
		value = 50
	}
}