

modify_bus_support = {
	add_to_variable = { bus = modify_bus }

	custom_effect_tooltip = bus_TT

	clamp_variable = {
		var = bus
		min = 0
		max = 25000
	}
}


	
