set_leader_SMA = {
	if = { limit = { has_country_flag = set_Neutral_Muslim_Brotherhood }
	if = { limit = { check_variable = { Neutral_Muslim_Brotherhood_leader = 0 } }
			add_to_variable = { Neutral_Muslim_Brotherhood_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Papa Sheep"
				picture = "papa_sheep.dds"
				ideology = Neutral_Muslim_Brotherhood
				traits = {
					neutrality_Neutral_Muslim_Brotherhood
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Muslim_Brotherhood_leader = 1 } }
			if = { limit = { date < 2069.1.2 } set_temp_variable = { b = 1 } } 
		}
	}
}