# Scripted effects for CSTO
CSTO_join = {
	add_to_array = { global.CSTO_member = THIS }
	add_ideas = CSTO_member
	ROOT = { add_to_tech_sharing_group = CSTO_Tech_Share }
	random_country = {
		limit = {
			has_idea = CSTO_member
			is_faction_leader = yes
		}
		add_to_faction = ROOT
	}
	hidden_effect = { news_event = { id = CSTO.4 hours = 6 } }
}

CSTO_leave = {
	remove_from_array = { global.CSTO_member = THIS }
	remove_ideas = CSTO_member
	ROOT = { remove_from_tech_sharing_group = CSTO_Tech_Share }
	leave_faction = yes
	hidden_effect = { news_event = { id = CSTO.5 hours = 6 } }
}
