defined_text = {
	name = party_index_L

	text = {
		trigger = { check_variable = { party_index = 0 } }
		localization_key = "[Western_Autocracy_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 1 } }
		localization_key = "[conservatism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 2 } }
		localization_key = "[liberalism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 3 } }
		localization_key = "[socialism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 4 } }
		localization_key = "[Communist-State_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 5 } }
		localization_key = "[anarchist_communism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 6 } }
		localization_key = "[Conservative_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 7 } }
		localization_key = "[Autocracy_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 8 } }
		localization_key = "[Mod_Vilayat_e_Faqih_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 9 } }
		localization_key = "[Vilayat_e_Faqih_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 10 } }
		localization_key = "[Kingdom_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 11 } }
		localization_key = "[Caliphate_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 12 } }
		localization_key = "[Neutral_Muslim_Brotherhood_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 13 } }
		localization_key = "[Neutral_Autocracy_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 14 } }
		localization_key = "[Neutral_conservatism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 15 } }
		localization_key = "[oligarchism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 16 } }
		localization_key = "[Neutral_Libertarian_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 17 } }
		localization_key = "[Neutral_green_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 18 } }
		localization_key = "[neutral_Social_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 19 } }
		localization_key = "[Neutral_Communism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 20 } }
		localization_key = "[Nat_Populism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 21 } }
		localization_key = "[Nat_Fascism_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 22 } }
		localization_key = "[Nat_Autocracy_L]"
	}
	text = {
		trigger = { check_variable = { party_index = 23 } }
		localization_key = "[Monarchist_L]"
	}
}

defined_text = {
	name = eu_parties_l

	text = {
		trigger = { check_variable = { party_index = 0 } }
		localization_key = "EU_parliament_PG_Western_Autocracy_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 1 } }
		localization_key = "EU_parliament_PG_conservatism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 2 } }
		localization_key = "EU_parliament_PG_liberalism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 3 } }
		localization_key = "EU_parliament_PG_socialism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 4 } }
		localization_key = "EU_parliament_PG_Communist-State_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 5 } }
		localization_key = "EU_parliament_PG_anarchist_communism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 6 } }
		localization_key = "EU_parliament_PG_Conservative_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 7 } }
		localization_key = "EU_parliament_PG_Autocracy_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 8 } }
		localization_key = "EU_parliament_PG_Mod_Vilayat_e_Faqih_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 9 } }
		localization_key = "EU_parliament_PG_Vilayat_e_Faqih_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 10 } }
		localization_key = "EU_parliament_PG_Kingdom_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 11 } }
		localization_key = "EU_parliament_PG_Caliphate_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 12 } }
		localization_key = "EU_parliament_PG_Neutral_Muslim_Brotherhood_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 13 } }
		localization_key = "EU_parliament_PG_Neutral_Autocracy_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 14 } }
		localization_key = "EU_parliament_PG_Neutral_conservatism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 15 } }
		localization_key = "EU_parliament_PG_oligarchism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 16 } }
		localization_key = "EU_parliament_PG_Neutral_Libertarian_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 17 } }
		localization_key = "EU_parliament_PG_Neutral_green_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 18 } }
		localization_key = "EU_parliament_PG_neutral_Social_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 19 } }
		localization_key = "EU_parliament_PG_Neutral_Communism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 20 } }
		localization_key = "EU_parliament_PG_Nat_Populism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 21 } }
		localization_key = "EU_parliament_PG_Nat_Fascism_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 22 } }
		localization_key = "EU_parliament_PG_Nat_Autocracy_short_name_loc_key"
	}
	text = {
		trigger = { check_variable = { party_index = 23 } }
		localization_key = "EU_parliament_PG_Monarchist_short_name_loc_key"
	}
}

defined_text = {
	name = term_limit_text
	text = {
		trigger = { check_variable = { term_limit > 0 } }
		localization_key = term_limit_text
	}
}