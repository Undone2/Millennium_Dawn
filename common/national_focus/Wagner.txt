## WAGNER FOCUS TREE
## Author: LordBogdanoff
focus_tree = {
	id = wagner_focus
	country = {
		base = 0
		modifier = {
			add = 25
			original_tag = WAG
		}
	}
	continuous_focus_position = { x = 3200 y = 2100 }

	focus = {
		id = WAG_Utkins_Orchestra
		icon = sov_wagner_prigozhin
		x = 0
		y = 2
		available = {
		}
		cost = 0.01
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_Utkins_Orchestra"
			custom_effect_tooltip = SOV_wagner_prigozin_TT
			hidden_effect = { add_dynamic_modifier = { modifier = WAG_wagner_politic_modifier } }
			add_to_variable = { WAG_wagner_politic_political_power_factor = 0.05 }
			add_to_variable = { WAG_wagner_politic_drift_defence_factor = 0.07 }
			add_to_variable = { WAG_wagner_politic_nationalist_drift = 0.03 }
			add_to_variable = { WAG_wagner_politic_stability_factor = -0.05 }
			custom_effect_tooltip = WAG_wagner_politic_start_tt

		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = WAG_africa_start
		icon = sov_wagner_sahel
		x = 12
		y = 1
		available = {
			has_country_flag = wagner_africa
		}
		cost = 0.1
		relative_position_id = WAG_Utkins_Orchestra
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_Utkins_Orchestra }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_africa_start"
			add_political_power = 100
			country_event = russia_wagner.37
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_council
		icon = wagner
		available = {
			NOT = {
				has_war_with = SOV
			}
		}
		x = 4
		y = 1
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_Utkins_Orchestra }
		relative_position_id = WAG_Utkins_Orchestra
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_council"
			add_to_variable = { WAG_wagner_politic_max_planning_factor = 0.05 }
			add_to_variable = { WAG_wagner_politic_planning_speed = 0.15 }
			custom_effect_tooltip = WAG_wagner_politic6_tt
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_use_trolls
		icon = sov_wagner_use_trolls
		x = 12
		y = 2
		available = {
			has_country_flag = wagner_africa
		}
		cost = 5
		prerequisite = { focus = WAG_africa_start }
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Utkins_Orchestra
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_use_trolls"
			add_stability = 0.03
			if = {
				limit = { country_exists = NGR }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = WAG }
				set_temp_variable = { influence_target = NGR }
				change_influence_percentage = yes
			}
			if = {
				limit = { country_exists = SUD }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = WAG }
				set_temp_variable = { influence_target = SUD }
				change_influence_percentage = yes
			}
			if = {
				limit = { country_exists = CAR }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = WAG }
				set_temp_variable = { influence_target = CAR }
				change_influence_percentage = yes
			}
			if = {
				limit = { country_exists = BFA }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = WAG }
				set_temp_variable = { influence_target = BFA }
				change_influence_percentage = yes
			}
			if = {
				limit = { country_exists = CHA }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = WAG }
				set_temp_variable = { influence_target = CHA }
				change_influence_percentage = yes
			}
			if = {
				limit = { country_exists = MAL }
				set_temp_variable = { percent_change = 3 }
				set_temp_variable = { tag_index = WAG }
				set_temp_variable = { influence_target = MAL }
				change_influence_percentage = yes
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_resruit_locals
		icon = sov_wagner_recruit_locals
		x = 5
		y = 2
		cost = 6
		prerequisite = { focus = WAG_council }
		relative_position_id = WAG_Utkins_Orchestra
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_resruit_locals"
			set_temp_variable = { modify_wagner = 1500 }
			modify_wagner_support = yes
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_niger_expansion
		icon = attack_niger
		x = -5
		y = 1
		available = {
			has_country_flag = wagner_africa
		}
		bypass = {
			NOT = { country_exists = NGR }
		}
		cost = 7
		prerequisite = { focus = WAG_use_trolls }
		relative_position_id = WAG_use_trolls
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		will_lead_to_war_with = NGR
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_niger_expansion"
			if = {
				limit = { NGR = { is_subject_of = SOV } }
				country_event = { id = russia_wagner.28  days = 1 }
			}
			if = {
				limit = { NGR = { is_subject_of = WAG } }
				WAG = { annex_country = { target = NGR transfer_troops = yes } }
			}
			if = {
				limit = { NOT = { NGR = { is_subject_of = SOV } } }
				create_wargoal = {
					target = NGR
					type = annex_everything
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_citizen_idea
		icon = MEX_Rewrite_the_constitution
		x = 2
		y = 2
		available = {
			has_country_flag = wagner_africa

		}
		cost = 3
		relative_position_id = WAG_use_trolls
		prerequisite = { focus = WAG_use_trolls }
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_citizen_idea"
			add_to_variable = { WAG_wagner_politic_stability_factor = 0.15 }
			add_to_variable = { WAG_wagner_politic_bureaucracy_cost_multiplier_modifier = 0.10 }
			custom_effect_tooltip = WAG_wagner_politic8_tt
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_political_inst
		icon = stolen_papers2
		x = 0
		y = 3
		available = {
			has_country_flag = wagner_africa
		}
		cost = 7
		prerequisite = { focus = WAG_citizen_idea focus = WAG_bfa_expansion focus = WAG_car_expansion }
		relative_position_id = WAG_use_trolls
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_political_inst"
			add_political_power = 150
			add_stability = 0.1
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_passport
		icon = sov_wagner_sahel_passport
		x = 0
		y = 1
		available = {
			has_country_flag = wagner_africa
		}
		cost = 7
		prerequisite = { focus = WAG_political_inst }
		relative_position_id = WAG_political_inst
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_passport"
			custom_effect_tooltip = WAG_passports_tt
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_car_expansion
		icon = attack_car
		x = 8
		y = 4
		available = {
			has_country_flag = wagner_africa
		}
		bypass = {
			NOT = { country_exists = CAR }
		}
		cost = 7
		relative_position_id = WAG_Utkins_Orchestra
		prerequisite = { focus = WAG_chad_expansion  focus = WAG_suda_expansion }
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		will_lead_to_war_with = CAR
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_car_expansion"
			if = {
				limit = { CAR = { is_subject_of = SOV } }
				country_event = { id = russia_wagner.29  days = 1 }
			}
			if = {
				limit = { CAR = { is_subject_of = WAG } }
				WAG = { annex_country = { target = CAR transfer_troops = yes } }
			}
			if = {
				limit = { NOT = { CAR = { is_subject_of = SOV } } }
				create_wargoal = {
					target = CAR
					type = annex_everything
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_bfa_expansion
		icon = attack_burkina_faso
		x = 6
		y = 4
		available = {
			has_country_flag = wagner_africa

		}
		bypass = {
			NOT = { country_exists = BFA }
		}
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		prerequisite = { focus = WAG_chad_expansion focus = WAG_niger_expansion }
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		will_lead_to_war_with = BFA
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_bfa_expansion"
			if = {
				limit = { BFA = { is_subject_of = SOV } }
				country_event = { id = russia_wagner.31  days = 1 }
			}
			if = {
				limit = { BFA = { is_subject_of = WAG } }
				WAG = { annex_country = { target = BFA transfer_troops = yes } }
			}
			if = {
				limit = { NOT = { BFA = { is_subject_of = SOV } } }
				create_wargoal = {
					target = BFA
					type = annex_everything
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_chad_expansion
		icon = attack_chad
		x = -3
		y = 1
		available = {
			has_country_flag = wagner_africa

		}
		bypass = {
			NOT = { country_exists = CHA }
		}
		cost = 7
		relative_position_id = WAG_use_trolls
		prerequisite = { focus = WAG_car_expansion focus = WAG_suda_expansion }
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		will_lead_to_war_with = CHA
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_bfa_expansion"
			if = {
				limit = { CHA = { is_subject_of = SOV } }
				country_event = { id = russia_wagner.32  days = 1 }
			}
			if = {
				limit = { CHA = { is_subject_of = WAG } }
				WAG = { annex_country = { target = CHA transfer_troops = yes } }
			}
			if = {
				limit = { NOT = { CHA = { is_subject_of = SOV } } }
				create_wargoal = {
					target = CHA
					type = annex_everything
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_suda_expansion
		icon = attack_sudan
		x = 0
		y = 1
		available = {
			has_country_flag = wagner_africa
		}
		bypass = {
			NOT = { country_exists = SUD }
		}
		cost = 7
		prerequisite = { focus = WAG_use_trolls }
		relative_position_id = WAG_use_trolls
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		will_lead_to_war_with = SUD
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_suda_expansion"
			if = {
				limit = { SUD = { is_subject_of = SOV } }
				country_event = { id = russia_wagner.30  days = 1 }
			}
			if = {
				limit = { SUD = { is_subject_of = WAG } }
				WAG = { annex_country = { target = SUD transfer_troops = yes } }
			}
			if = {
				limit = { NOT = { SUD = { is_subject_of = SOV } } }
				create_wargoal = {
					target = SUD
					type = annex_everything
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_anti_bunts2
		icon = sov_wagner_bunt
		x = 3
		y = 2
		cost = 7
		prerequisite = { focus = WAG_council }
		relative_position_id = WAG_Utkins_Orchestra
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_anti_bunts"
			add_to_variable = { WAG_wagner_politic_resistance_damage_to_garrison = -0.10 }
			add_to_variable = { WAG_wagner_politic_resistance_growth = -0.15 }
			custom_effect_tooltip = WAG_wagner_politic7_tt
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_dsrg_rusich_poland_volunteer
		icon = sov_wagner_polish_rusich
		x = -3
		y = 2
		available = {
			has_war_with = POL
		}
		prerequisite = { focus = WAG_dsrg_rusich }
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Utkins_Orchestra
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_dsrg_rusich_poland_volunteer"
			random_owned_controlled_state = {
				prioritize = { 1014 }
				create_unit = {
					division = "name = \"DSHRG Rusich\" division_template = \"Battalion of Russian Nationalists\" start_experience_factor = 1.0"
					owner = WAG
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_The_March_of_Freedom
		icon = sov_wagner_march
		x = -14
		y = 1
		available = {
			has_war_with = SOV
		}
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Utkins_Orchestra
		prerequisite = { focus = WAG_Utkins_Orchestra }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_The_March_of_Freedom"
			667 = {
				add_dynamic_modifier = {
				modifier = WAG_Bunt_modifer
				scope = WAG
				days = 7
			}
			}
			667 = {
				add_dynamic_modifier = {
				modifier = RUS_WAG_Bunt_modifer
				scope = SOV
				days = 10
			}
		 }
		 random_owned_controlled_state = {
			prioritize = { 667 }
			create_unit = {
				division = "name = \"Wagner Light Unit\" division_template = \"Wagner Light Unit\" start_experience_factor = 1.0"
				owner = WAG
			}
		}
		random_owned_controlled_state = {
			prioritize = { 667 }
			create_unit = {
				division = "name = \"Wagner Light Unit\" division_template = \"Wagner Light Unit\" start_experience_factor = 1.0"
				owner = WAG
			}
		}
		random_owned_controlled_state = {
			prioritize = { 667 }
			create_unit = {
				division = "name = \"Wagner Group PMC\" division_template = \"Wagner Group PMC\" start_experience_factor = 1.0"
				owner = WAG
			}
		}
		random_owned_controlled_state = {
			prioritize = { 667 }
			create_unit = {
				division = "name = \"Wagner Tank Unit\" division_template = \"Wagner Tank Unit\" start_experience_factor = 1.0"
				owner = WAG
			}
		}
		 news_event = {
			hours = 3
			id = russia_wag_news.1
		}
		}
		ai_will_do = { factor = 350 }
	}
	focus = {
		id = WAG_Join__us
		icon = WAG_Join_us_icon
		x = 0
		y = 1
		available = {
			has_war_with = SOV
		}
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_The_March_of_Freedom
		prerequisite = { focus = WAG_The_March_of_Freedom }
		completion_reward = {
			clr_country_flag = dynamic_flag
			if = {
				limit = {
					country_exists = CHE
					CHE = { is_subject_of = SOV }
				}
				CHE = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = LPR
					LPR = { is_subject_of = SOV }
				}
				LPR = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = HPR
					HPR = { is_subject_of = SOV }
				}
				HPR = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = OPR
					OPR = { is_subject_of = SOV }
				}
				OPR = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = BSH
					BSH = { is_subject_of = SOV }
				}
				BSH = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = DPR
					DPR = { is_subject_of = SOV }
				}
				DPR = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = TAT
					TAT = { is_subject_of = SOV }
				}
				TAT = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = YAK
					YAK = { is_subject_of = SOV }
				}
				YAK = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = DAG
					DAG = { is_subject_of = SOV }
				}
				DAG = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = KAE
					KAE = { is_subject_of = SOV }
				}
				KAE = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = BRY
					BRY = { is_subject_of = SOV }
				}
				BRY = { country_event = { id = russia_wagner.22 days = 1 } }
			}
			if = {
				limit = {
					country_exists = CRM
					CRM = { is_subject_of = SOV }
				}
				CRM = { country_event = { id = russia_wagner.22 days = 1 } }
			}
		}
	}
	focus = {
		id = WAG_zek_call
		icon = sov_wagner_storm_z
		x = 2
		y = 1
		available = {
			has_war_with = SOV
		}
		prerequisite = { focus = WAG_Join__us }
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Join__us
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_zek_call"
			custom_effect_tooltip = WAG_zekes_TT
			hidden_effect = {
				SOV = {
					delete_unit_template_and_units = {
					division_template = "Battalion of Prisoners"
					disband = yes
					}
				}
			division_template = {
				name = "Battalion of Prisoners"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 1 y = 0 }

				}
				priority = 2
			}
			random_owned_controlled_state = {
				prioritize = { 665 }
				create_unit = {
					division = "name = \"Battalion of Prisoners\" division_template = \"Battalion of Prisoners\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Battalion of Prisoners\" division_template = \"Battalion of Prisoners\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Battalion of Prisoners\" division_template = \"Battalion of Prisoners\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Battalion of Prisoners\" division_template = \"Battalion of Prisoners\" start_experience_factor = 0.1"
					owner = ROOT
				}
			}
		}
	}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_Border_guards
		icon = WAG_Border_guards_icon
		x = -2
		y = 1
		available = {
			has_war_with = SOV
		}
		prerequisite = { focus = WAG_Join__us }
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Join__us
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_Border_guards"
			custom_effect_tooltip = WAG_zekes_TT
			hidden_effect = {
				SOV = {
					delete_unit_template_and_units = {
					division_template = "Battalion of Prisoners"
					disband = yes
					}
				}
			division_template = {
				name = "Border guards"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Militia_Bat = { x = 1 y = 0 }

				}
				priority = 2
			}
			WAG = {
				set_province_controller = 6432
				set_province_controller = 473
				set_province_controller = 11412	
				set_province_controller = 6591
			}	
			random_owned_controlled_state = {
				prioritize = { 667 }
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
			}
			random_owned_controlled_state = {
				prioritize = { 656 }
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
				create_unit = {
					division = "name = \"Border guards\" division_template = \"Border guards\" start_experience_factor = 0.1"
					owner = ROOT
				}
			}
			WAG = {
				country_event = russia_March_of_Justice.2
			}
			SOV = {
				country_event = russia_March_of_Justice.2
			}
		}
	}
	ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_to_attract_RUSICH
		icon = WAG_to_attract_RUSICH_icon
		x = 0
		y = 1
		available = {
			has_war_with = SOV
		}
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Join__us
		prerequisite = { focus = WAG_Join__us }
		completion_reward = { 
			log = "[GetDateText]: [Root.GetName]: Focus WAG_to_attract_RUSICH"
			division_template = {
				name = "DSHRG Rusich"
				regiments = {
					Militia_Bat = { x = 0 y = 0 }
					Militia_Bat = { x = 0 y = 1 }
					Special_Forces = { x = 1 y = 0 }
					Special_Forces = { x = 1 y = 1 }
				}
				priority = 2
			}
			random_owned_controlled_state = {
				prioritize = { 667 }
				create_unit = {
					division = "name = \"DSHRG Rusich\" division_template = \"DSHRG Rusich\" start_experience_factor = 1.0"
					owner = WAG
				}
			}
		}
	}
	focus = {
		id = WAG_vacation_in_Voronezh
		icon = WAG_vacation_in_Voronezh_icon
		x = 0
		y = 1
		available = {
			has_war_with = SOV
			controls_province = 9417 
		}
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_to_attract_RUSICH
		prerequisite = { focus = WAG_to_attract_RUSICH }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_The_March_of_Freedom"
			656 = {
				add_dynamic_modifier = {
				modifier = WAG_Bunt_modifer
				scope = WAG
				days = 20
				}
			}
			656 = {
				add_dynamic_modifier = {
				modifier = RUS_WAG_Bunt_modifer
				scope = SOV
				days = 10
				}
			}
			657 = {
				add_dynamic_modifier = {
				modifier = RUS_WAG_Bunt_modifer
				scope = SOV
				days = 10
				}
			}
			SOV = {
				country_event = russia_March_of_Justice.3
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_Begin_the_assault_on_Moscow
		icon = WAG_Begin_the_assault_on_Moscow_icon
		x = 0
		y = 1
		available = {
			has_war_with = SOV
			controls_province = 6230
		}
		prerequisite = { focus = WAG_vacation_in_Voronezh }
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_vacation_in_Voronezh
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_Begin_the_assault_on_Moscow"
			655 = {
				add_dynamic_modifier = {
				modifier = RUS_WAG_Bunt_modifer
				scope = SOV
				days = 20
				}
			}
			country_event = russia_March_of_Justice.1
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_done_with_pipa
		icon = wagner
		x = 0
		y = 2
		available = {
			has_war_with = SOV
			controls_state = 652
		}
		prerequisite = { focus = WAG_Begin_the_assault_on_Moscow }
		cost = 0.1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_vacation_in_Voronezh
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_done_with_pipa"
			SOV = {country_event = russia_March_of_Justice.4}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_patriots_werbovka
		icon = the_z_apc
		x = -7
		y = 1
		available = {
			has_war_with = SOV
		}
		prerequisite = {  focus = WAG_Utkins_Orchestra }
		cost = 3
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Utkins_Orchestra
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_patriots_werbovka"
			add_manpower = 2000
			SOV = { add_manpower = -2000 }
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_dsrg_rusich
		icon = sov_wagner_rusich
		x = -3
		y = 1
		available = {
			NOT = { has_war_with = SOV }
			SOV = {
				has_country_flag = rusich_create
			}
		}
		prerequisite = { focus = WAG_Utkins_Orchestra }
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_Utkins_Orchestra
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_dsrg_rusich"
			custom_effect_tooltip = WAG_rusich_tt
			hidden_effect = {
			SOV = {
				delete_unit_template_and_units = {
					division_template = "Battalion of Russian Nationalists"
					disband = yes
				}
			}
			division_template = {
				name = "Battalion of Russian Nationalists"
				is_locked = yes
				regiments = {
					Special_Forces = { x = 0 y = 0 }
					Special_Forces = { x = 0 y = 1 }
				}

				priority = 2
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 666 }
				create_unit = {
					division = "name = \"DSHRG Rusich\" division_template = \"DSHRG Rusich\" start_experience_factor = 1.0"
					owner = WAG
				}
			}
		}
	}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_moa
		icon = sov_moa
		x = 2
		y = 1
		available = {
			has_war_with = SOV
			NOT = { country_exists = MLR }
			SOV = {
				has_country_flag = moa_create
			}
		}
		prerequisite = { focus = WAG_patriots_werbovka }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_patriots_werbovka
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_moa"
			custom_effect_tooltip = WAG_MOA_tt
			hidden_effect = {
			SOV = {
				delete_unit_template_and_units = {
					division_template = "MaloRussian Liberation Army"
					disband = yes
				}
			}
			division_template = {
				name = "MaloRussian Liberation Army"
				is_locked = yes
				regiments = {
					Special_Forces = { x = 0 y = 0 }
					Special_Forces = { x = 0 y = 0 }
					Special_Forces = { x = 0 y = 1 }
					Special_Forces = { x = 0 y = 2 }
					Special_Forces = { x = 1 y = 0 }
					Special_Forces = { x = 1 y = 1 }
					Special_Forces = { x = 1 y = 2 }
				}
				support = {
					L_Recce_Comp = { x = 0 y = 0 }
					L_Engi_Comp = { x = 0 y = 1 }
				}
				priority = 2
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 666 }
				create_unit = {
					division = "name = \"MaloRussian Liberation Army\" division_template = \"MaloRussian Liberation Army\" start_experience_factor = 1.0"
					owner = WAG
				}
			}
		}
	}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_werbovka_navalniy
		icon = sov_wagner_navalny
		x = -2
		y = 1
		available = {
			SOV = {
				NOT = {
					OR = {
					western_liberals_are_in_power = yes
					western_conservatism_are_in_power = yes
					western_social_democrats_are_in_power = yes
					}
				}
			}
			has_war_with = SOV
			controls_state = 651
		}
		prerequisite = { focus = WAG_patriots_werbovka }
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		relative_position_id = WAG_patriots_werbovka
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_werbovka_navalniy"
			add_manpower = 500
			SOV = { add_manpower = -499 }
			country_event = { id = russia_wagner.23 days = 1 }
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = WAG_werbovka_nationalist
		icon = sov_jirik_coop_nazi
		x = 0
		y = 1
		available = {
			has_war_with = SOV
		}
		prerequisite = { focus = WAG_patriots_werbovka }
		relative_position_id = WAG_patriots_werbovka
		cost = 1
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_werbovka_nationalist"
			custom_effect_tooltip = WAG_nationalist_tt
			hidden_effect = {
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 666 }
				create_unit = {
					division = "name = \"PMC Hispaniola\" division_template = \"Battalion of Russian Nationalists\" start_experience_factor = 0.2"
					owner = WAG
				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 666 }
				create_unit = {
					division = "name = \"Russian National Unity\" division_template = \"Battalion of Russian Nationalists\" start_experience_factor = 0.2"
					owner = WAG
				}
			}
			}
		}
		ai_will_do = { factor = 100 }
	}

	focus = {
		id = WAG_wagner_centers
		icon = sov_wagner_centr
		available = {
			NOT = {
				has_war_with = SOV
			}
		}
		x = 2
		y = 1
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_Utkins_Orchestra }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_centers"
			two_office_construction = yes
		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_wagner_ideology
		icon = sov_wagner_jambo
		available = {
			NOT = {
				has_war_with = SOV
			}
		}
		x = -1
		y = 1
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_Utkins_Orchestra }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_ideology"
			add_to_variable = { WAG_wagner_politic_political_power_factor = 0.01 }
			add_to_variable = { WAG_wagner_politic_drift_defence_factor = 0.01 }
			add_to_variable = { WAG_wagner_politic_nationalist_drift = 0.02 }
			add_to_variable = { WAG_wagner_politic_stability_factor = -0.02 }
			add_to_variable = { WAG_wagner_politic_conversion_cost_civ_to_mil_factor = -0.30 }
			custom_effect_tooltip = WAG_wagner_politic1_tt

		}
		ai_will_do = { factor = 1 }
	}
	focus = {
		id = WAG_wagner_invansion_principle
		icon = war3
		x = 1
		y = 3
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_wagner_vagneryata }
			completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_invansion_principle"
			if = {
				limit = { has_idea = intervention_limited_interventionism }
				swap_ideas = {
					remove_idea = intervention_limited_interventionism
					add_idea = intervention_neo_imperialism
				}
			}
			else_if = {
				limit = { has_idea = intervention_local_security }
				swap_ideas = {
					remove_idea = intervention_local_security
					add_idea = intervention_neo_imperialism
				}
			}
			else_if = {
				limit = { has_idea = intervention_regional_interventionism }
				swap_ideas = {
					remove_idea = intervention_regional_interventionism
					add_idea = intervention_neo_imperialism
				}
			}
			else_if = {
				limit = { has_idea = intervention_global_interventionism }
				swap_ideas = {
					remove_idea = intervention_global_interventionism
					add_idea = intervention_neo_imperialism
				}
			}
			add_to_variable = { WAG_wagner_politic_drift_defence_factor = 0.01 }
			add_to_variable = { WAG_wagner_politic_send_volunteers_tension = -0.30 }
			custom_effect_tooltip = WAG_wagner_politic3_tt
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = WAG_wagner_vagneryata
		icon = sov_wagner_wagnerenok
		x = 0
		y = 2
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_wagner_ideology }
		prerequisite = { focus = WAG_wagner_centers }
		  	completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_vagneryata"
			add_to_variable = { WAG_wagner_politic_drift_defence_factor = 0.01 }
			add_to_variable = { WAG_wagner_politic_stability_factor = -0.02 }
			add_to_variable = { WAG_wagner_politic_conscription_factor = 0.05 }
			custom_effect_tooltip = WAG_wagner_politic2_tt

		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = WAG_wagner_foreign_legion
		icon = sov_wagner_foreign_legion
		x = -1
		y = 3
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_wagner_vagneryata }
		  	completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_foreign_legion"
			set_temp_variable = { modify_wagner = 1500 }
			modify_wagner_support = yes
			set_temp_variable = { treasury_change = -10 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = WAG_wagner_outher_heaven
		icon = sov_wagner_outer_heaven
		x = 0
		y = 4
		relative_position_id = WAG_Utkins_Orchestra
		cost = 7
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_wagner_foreign_legion }
		prerequisite = { focus = WAG_wagner_invansion_principle }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_outher_heaven"
			unlock_decision_category_tooltip = WAG_wagner_mother_base_category

		}
		ai_will_do = { factor = 20 }
	}
	focus = {
		id = WAG_wagner_bank
		icon = wagner_bank
		x = 0
		y = 5
		relative_position_id = WAG_Utkins_Orchestra
		cost = 5
		search_filters = { FOCUS_FILTER_MILITARY_LAWS }
		prerequisite = { focus = WAG_wagner_outher_heaven }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus WAG_wagner_bank"
			increase_economic_growth = yes
		}

		ai_will_do = { factor = 20 }
	}

}


