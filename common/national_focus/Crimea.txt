#Made by Lord Bogdanoff
focus_tree = {
	id = crimea_focus
	country = {
		factor = 0
		modifier = {
			add = 20
			original_tag = CRM
		}
	}
	continuous_focus_position = { x = 2000 y = 1100 }
	focus = {
		id = CRM_economic
		icon = goal_JAP_valueeven
		x = 7
		y = 7
		available = {
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_economic"
			increase_economic_growth = yes
			calculate_interest_rate = yes
		}
	}
	focus = {
		id = CRM_invest
		icon = MEX_Social_investments
		x = -2
		y = 1
		available = {
			owns_state = 669
		}
		prerequisite = { focus = CRM_economic }
		relative_position_id = CRM_economic
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_invest"
			set_temp_variable = { treasury_change = 3.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 1 }
			set_temp_variable = { tag_index = UKR }
			set_temp_variable = { influence_target = CRM }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 1 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = CRM }
			change_influence_percentage = yes
		}
	}
	focus = {
		id = CRM_pishevaya
		icon = vineyard
		x = 0
		y = 1
		available = {
			owns_state = 669
		}
		prerequisite = { focus = CRM_economic }
		relative_position_id = CRM_economic
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_pishevaya"
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
		}
	}
	focus = {
		id = CRM_trolleybus
		icon = egypt_bus
		x = -1
		y = 2
		available = {
			owns_state = 669
		}
		prerequisite = { focus = CRM_pishevaya }
		prerequisite = { focus = CRM_invest }
		relative_position_id = CRM_economic
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_trolleybus"
			add_ideas = CRM_buses
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_evpator
		icon = army_tank_industry
		x = 0
		y = 3
		available = {
			owns_state = 669
		}
		prerequisite = { focus = CRM_selhoz_product }
		prerequisite = { focus = CRM_trolleybus }
		relative_position_id = CRM_economic
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_trolleybus"
			one_random_arms_factory = yes
		}
	}
	focus = {
		id = CRM_selhoz_product
		icon = agriculture
		x = 1
		y = 2
		available = {
			owns_state = 669
		}
		prerequisite = { focus = CRM_pishevaya }
		prerequisite = { focus = CRM_neftegaz }
		relative_position_id = CRM_economic
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_selhoz_product"
			add_ideas = CRM_agriculture
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_neftegaz
		icon = generic_gas
		x = 2
		y = 1
		prerequisite = { focus = CRM_economic }
		relative_position_id = CRM_economic
		ai_will_do = { factor = 20 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_neftegaz"
			random_core_state = {
				add_resource = {
					type = oil
					amount = 1
				}
			}
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_samooborona
		icon = crm_self_defence
		x = 2
		y = 7
		available = {
			NOT = { is_subject_of = UKR }
		}
		ai_will_do = { factor = 200 }
		cost = 5
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_samooborona"
			add_ideas = CRM_samoborona_idea
			set_temp_variable = { percent_change = 5 }
			change_domestic_influence_percentage = yes
		}
	}
	focus = {
		id = CRM_anti_ukr
		icon = attack_ukraine
		x = -1
		y = 1
		available = {
			NOT = { is_subject_of = UKR }
		}
		ai_will_do = { factor = 300 }
		cost = 5
		prerequisite = { focus = CRM_samooborona }
		relative_position_id = CRM_samooborona
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_anti_ukr"
			set_temp_variable = { percent_change = -10 }
			set_temp_variable = { tag_index = UKR }
			set_temp_variable = { influence_target = CRM }
			change_influence_percentage = yes
		}
	}
	focus = {
		id = CRM_defend_armyansk
		icon = military_fort2
		x = 1
		y = 1
		available = {
			NOT = { is_subject_of = UKR }
			owns_state = 669
		}
		ai_will_do = { factor = 300 }
		cost = 5
		prerequisite = { focus = CRM_samooborona }
		relative_position_id = CRM_samooborona
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_defend_armyansk"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			669 = {
				add_building_construction = {
					type = bunker
					province = 3722
					level = 2
					instant_build = yes
				}
			}
		}
	}
	focus = {
		id = CRM_became_opolchenie
		icon = crm_militia
		x = 0
		y = 2
		available = {
			NOT = { is_subject_of = UKR }
			owns_state = 669
		}
		ai_will_do = { factor = 300 }
		cost = 5
		prerequisite = { focus = CRM_defend_armyansk }
		prerequisite = { focus = CRM_anti_ukr }
		relative_position_id = CRM_samooborona
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_became_opolchenie"
			swap_ideas = {
				remove_idea = CRM_samoborona_idea
				add_idea = CRM_samoborona_idea1
			}
			set_temp_variable = { percent_change = 10 }
			change_domestic_influence_percentage = yes
			division_template = {
				name = "People's Militia Battalion"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
					}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 669 }
				create_unit = {
					division = "name = \"People's Militia Battalion\" division_template = \"People's Militia Battalion\" start_experience_factor = 0.1"
					owner = CRM
				}
			}
		}
	}
	focus = {
		id = CRM_opolchenie_police
		icon = blr_police_training
		x = -1
		y = 3
		available = {
			NOT = { is_subject_of = UKR }
			owns_state = 669
		}
		ai_will_do = { factor = 300 }
		cost = 5
		prerequisite = { focus = CRM_became_opolchenie }
		relative_position_id = CRM_samooborona
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_opolchenie_police"
			swap_ideas = {
				remove_idea = CRM_samoborona_idea1
				add_idea = CRM_samoborona_idea2
			}
		}
	}
	focus = {
		id = CRM_opolchenie_corrupt
		icon = BLR_Anti_Corruption
		x = 1
		y = 3
		available = {
			NOT = { is_subject_of = UKR }
			owns_state = 669
		}
		ai_will_do = { factor = 300 }
		cost = 5
		prerequisite = { focus = CRM_became_opolchenie }
		relative_position_id = CRM_samooborona
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_opolchenie_police"
			decrease_corruption = yes
		}
	}
	focus = {
		id = CRM_natbols
		icon = National_Marxism
		x = -1
		y = 0
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 300 }
		cost = 1
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy"
			set_temp_variable = { party_index = 0 }
			set_temp_variable = { party_popularity_increase = 0.035 }
			set_temp_variable = { temp_outlook_increase = 0.035 }
			add_relative_party_popularity = yes
			set_temp_variable = { party_index = 21 }
			set_temp_variable = { party_popularity_increase = 0.025 }
			set_temp_variable = { temp_outlook_increase = 0.025 }
			add_relative_party_popularity = yes
			retire_country_leader = yes
			set_country_flag = SOV_subject_agree
			if = {
				limit = { has_idea = the_clergy }
				swap_ideas = {
					remove_idea = the_clergy
					add_idea = farmers
				}
			}
			if = {
				limit = { has_idea = oligarchs }
				swap_ideas = {
					remove_idea = oligarchs
					add_idea = the_military
				}
			}
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 21 } } }
				hidden_effect = {
					clear_array = ruling_party
					clear_array = gov_coalition_array
					add_to_array = { ruling_party = 21 }
					update_government_coalition_strength = yes
					update_party_name = yes
					set_coalition_drift = yes
					meta_effect = {
						text = {
							set_country_flag = [META_SET_RULING_PARTY]
						}
						META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
					}
				set_leader = yes
				}
				set_politics = {
					ruling_party = nationalist
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
				add_to_variable = { party_pop_array^23 = 0.075 }
				recalculate_party = yes
			}
			create_country_leader = {
				name = "Vladimir Bugarevskiy"
				picture = "gfx/leaders/SOV/CRM/vladimir_bugarevskiy.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
					pro_russia
					military_career
					permanent_revolutionary
				}
			}
			
		}
	}
	focus = {
		id = CRM_rus_nation
		icon = petrogradcircle
		x = 0
		y = 1
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 300 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_natbols }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_rus_nation"
			custom_effect_tooltip = CRM_tatars_bunt_tt
			hidden_effect = {
				create_dynamic_country = {
					original_tag = CRM
					copy_tag = CRM
					transfer_state = 669
					set_politics = {
						ruling_party = democratic
						elections_allowed = yes
					}
					set_popularities = {
						democratic = 80
						neutrality = 20
						fascism = 0
						communism = 0
						nationalist = 0
					}
					inherit_technology = CRM
					clear_array = ruling_party
					add_to_array = {
						ruling_party = 1
					}
					set_temp_variable = {
						current_cons_popularity = party_pop_array^1
					}
					recalculate_party = yes
					declare_war_on = {
						target = CRM
						type = annex_everything
					}
					country_event = subject_rus.35
					set_cosmetic_tag = CRM_tatars
					clr_country_flag = dynamic_flag
					create_country_leader = {
						name = "Refat Chubarov"
						picture = "refat_сhubarov.dds"
						expire = "2034.1.1"
						ideology = conservatism
						traits = { western_liberalism }
					}
				}
			}
		}
	}
	focus = {
		id = CRM_peredel
		icon = sov_anti_oligarch
		x = -1
		y = 2
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 300 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_rus_nation }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_peredel"
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
		}
	}
	focus = {
		id = CRM_orujie
		icon = sov_nazbol_weapon
		x = 1
		y = 2
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 300 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_rus_nation }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_orujie"
			add_ideas = CRM_nazbols
		}
	}
	focus = {
		id = CRM_no_aborts
		icon = stop_aborts
		x = 0
		y = 3
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_orujie }
		prerequisite = { focus = CRM_peredel }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_no_aborts"
			swap_ideas = {
				remove_idea = CRM_nazbols
				add_idea = CRM_nazbols1
			}
		}
	}
	focus = {
		id = CRM_national_science
		icon = nazbol_coop
		x = -1
		y = 4
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_no_aborts }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_national_science"
			swap_ideas = {
				remove_idea = CRM_nazbols1
				add_idea = CRM_nazbols2
			}
		}
	}
	focus = {
		id = CRM_support_other
		icon = nazbol_flag_hands
		x = 1
		y = 4
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_no_aborts }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_support_other"
			set_temp_variable = { temp_opinion = 15 }
			change_the_military_opinion = yes
			change_farmers_opinion = yes
			change_small_medium_business_owners_opinion = yes
		}
	}
	focus = {
		id = CRM_battle_suqads
		icon = sov_nazbol_military
		x = 0
		y = 5
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_national_science }
		prerequisite = { focus = CRM_support_other }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_battle_suqads"
			division_template = {
				name = "Combat Squads"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
					}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 669 }
				create_unit = {
					division = "name = \"Combat Squads of National Bolshevik\" division_template = \"Combat Squads\" start_experience_factor = 0.1"
					owner = ROOT
				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 669 }
				create_unit = {
					division = "name = \"Combat Squads of National Bolshevik\" division_template = \"Combat Squads\" start_experience_factor = 0.1"
					owner = ROOT
				}
			}
		}
	}
	focus = {
		id = CRM_army
		icon = focus_rus_yunarmiya
		x = -2
		y = 6
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_battle_suqads }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_army"
			swap_ideas = {
				remove_idea = CRM_nazbols2
				add_idea = CRM_nazbols3
			}
		}
	}
	focus = {
		id = CRM_ruralisation
		icon = breadbasket_of_the_union
		x = 0
		y = 6
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_battle_suqads }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_ruralisation"
			add_ideas = CRM_ruralisation
		}
	}
	focus = {
		id = CRM_new_religion
		icon = cult_of_stalin
		x = -1
		y = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_ruralisation }
		prerequisite = { focus = CRM_army }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_new_religion"
			swap_ideas = {
				remove_idea = orthodox_christian
				add_idea = CRM_russian_orthodox_communist_church_idea
			}
		}
	}
	focus = {
		id = CRM_rus_expansion
		icon = unkfgnown
		x = 1
		y = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_ruralisation }
		prerequisite = { focus = CRM_anti_west }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_rus_expansion"
			swap_ideas = {
				remove_idea = CRM_nazbols3
				add_idea = CRM_nazbols4
			}
		}
	}
	focus = {
		id = CRM_anti_west
		icon = sov_nazbol_anti_usa
		x = 2
		y = 6
		available = {
			nationalist_fascist_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_natbols
		prerequisite = { focus = CRM_battle_suqads }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_anti_west"
			add_ideas = CRM_anti_west
		}
	}
	focus = {
		id = CRM_monarchy
		icon = Parliament_monarchism
		x = 4
		y = 0
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 1
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy"
			set_temp_variable = { party_index = 0 }
			set_temp_variable = { party_popularity_increase = 0.035 }
			set_temp_variable = { temp_outlook_increase = 0.035 }
			add_relative_party_popularity = yes
			set_temp_variable = { party_index = 23 }
			set_temp_variable = { party_popularity_increase = 0.025 }
			set_temp_variable = { temp_outlook_increase = 0.025 }
			add_relative_party_popularity = yes
			retire_country_leader = yes
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 23 } } }
				hidden_effect = {
					clear_array = ruling_party
					clear_array = gov_coalition_array
					add_to_array = { ruling_party = 23 }
					update_government_coalition_strength = yes
					update_party_name = yes
					set_coalition_drift = yes
					meta_effect = {
						text = {
							set_country_flag = [META_SET_RULING_PARTY]
						}
						META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
					}
				set_leader = yes
				}
				set_politics = {
					ruling_party = nationalist
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
				add_to_variable = { party_pop_array^23 = 0.075 }
				recalculate_party = yes
			}
			create_country_leader = {
				name = "George I Romanov"
				picture = "gfx/leaders/SOV/CRM/georgy_romanov.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
					likeable
				}
			}
			if = {
				limit = { has_idea = farmers }
				swap_ideas = {
					remove_idea = farmers
					add_idea = the_clergy
				}
			}
		}
	}
	focus = {
		id = CRM_monarchy_rule
		icon = romanov_crest
		x = 0
		y = 1
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy"
			add_ideas = CRM_rule_romanov
		}
	}
	focus = {
		id = CRM_monarchy_constituion
		icon = pass_legislation
		x = -2
		y = 2
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_rule }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_constituion"
			add_political_power = 100
		}
	}
	focus = {
		id = CRM_monarchy_antidemocracy
		icon = authoritarian_democracy
		x = 0
		y = 2
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_rule }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_antidemocracy"
			add_popularity = {
				ideology = democratic
				popularity = -0.25
			}
			recalculate_party = yes
			add_stability = -0.08
			add_timed_idea = {
				idea = CRM_crusade_against_democracy
				days = 260
			}
		}
	}
	focus = {
		id = CRM_monarchy_church
		icon = orthodox_reform
		x = 2
		y = 2
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_rule }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_antidemocracy"
			set_temp_variable = { temp_opinion = 15 }
			change_the_clergy_opinion = yes
			change_oligarchs_opinion = yes
			add_political_power = 150
		}
	}
	focus = {
		id = CRM_monarchy_tzar
		icon = RUS_Tsar_Eternal
		x = 0
		y = 3
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_antidemocracy }
		prerequisite = { focus = CRM_monarchy_constituion }
		prerequisite = { focus = CRM_monarchy_church }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tzar"
			swap_ideas = {
				remove_idea = CRM_rule_romanov
				add_idea = CRM_rule_romanov1
			}
		}
	}
	focus = {
		id = CRM_monarchy_tzar_academy
		icon = research2
		x = -1
		y = 4
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_tzar }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tzar_academy"
			add_ideas = CRM_royal_academy
			
		}
	}
	focus = {
		id = CRM_monarchy_tzar_polk
		icon = Generic_Imperial_Russian_Soldier
		x = 1
		y = 4
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_tzar }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tzar_polk"
			set_temp_variable = { treasury_change = -2.00 }
			modify_treasury_effect = yes
			add_ideas = CRM_royal_leib_guard
			CRM = {
				division_template = {
					name = "Leib Polk"
					regiments = {
						Special_Forces = { x = 0 y = 0 }
						
					}
				}
			}
			capital_scope = {
				create_unit = {
					division = "name = \"His Majesty's Hussar Leib Guard Regiment\" division_template = \"Leib Polk\" start_experience_factor = 0.5"
					owner = CRM
					prioritize_location = 669
				}
			}
			
		}
	}
	focus = {
		id = CRM_decommusitaion
		icon = focus_SOV_behead_the_snake
		x = -2
		y = 5
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_tzar_polk }
		prerequisite = { focus = CRM_monarchy_tzar_academy }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tavria"
			add_timed_idea = { idea = CRM_idea_decommunization days = 1825 }
		}
	}
	focus = {
		id = CRM_zdravo
		icon = welfare
		x = 0
		y = 5
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_tzar_polk }
		prerequisite = { focus = CRM_monarchy_tzar_academy }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tavria"
			add_political_power = 45
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			increase_social_spending = yes
			increase_healthcare_budget = yes
		}
	}
	focus = {
		id = CRM_oligarch
		icon = gamble_cards
		x = 2
		y = 5
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_monarchy_tzar_polk }
		prerequisite = { focus = CRM_monarchy_tzar_academy }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tavria"
			set_temp_variable = { treasury_change = 7 }
			modify_treasury_effect = yes
			increase_corruption = yes
			set_temp_variable = { temp_opinion = 10 }
			change_oligarchs_opinion = yes
		}
	}
	focus = {
		id = CRM_monarchy_tavria
		icon = CRM_tavria
		x = 0
		y = 6
		available = {
			nationalist_monarchists_are_in_power = yes
		}
		ai_will_do = { factor = 20 }
		cost = 5
		relative_position_id = CRM_monarchy
		prerequisite = { focus = CRM_zdravo }
		prerequisite = { focus = CRM_decommusitaion }
		prerequisite = { focus = CRM_oligarch }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_monarchy_tavria"
			add_political_power = 150
			hidden_effect = {
				set_cosmetic_tag = CRM_Tavria
			}
			CRM = { add_state_core = 1085 }
			CRM = { add_state_core = 694 }
		}
	}
	focus = {
		id = CRM_Pro_russ
		icon = flag_russia
		x = 11
		y = 0
		available = {
			is_subject_of = SOV
		}
		ai_will_do = { factor = 20 }
		cost = 1
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_Pro_russ"
			add_opinion_modifier = { target = SOV modifier = recent_actions_positive }
			reverse_add_opinion_modifier = { target = SOV modifier = recent_actions_positive }
			set_temp_variable = { treasury_change = 5.0 }
			modify_treasury_effect = yes
			set_temp_variable = { receiver_nation = CRM.id }
			set_temp_variable = { sender_nation = SOV.id }
			set_improved_trade_agreement = yes
			set_temp_variable = { target_nation = CRM.id }
			set_temp_variable = { adding_nation = SOV.id }
			change_permanent_investment_target = yes
			set_temp_variable = { target_nation = SOV }
			set_temp_variable = { adding_nation = CRM }
			change_permanent_investment_target = yes
			if = {
				limit = { ROOT = { is_ai = yes } }
				country_event = { id = dpr.6 days = 1 }
			}
			if = {
				limit = { has_idea = the_clergy }
				swap_ideas = {
					remove_idea = the_clergy
					add_idea = farmers
				}
			}
			hidden_effect = {
				set_country_flag = SOV_subject_agree
				create_country_leader = {
					name = "Sergey Aksenov"
					picture = "gfx/leaders/SOV/CRM/sergey_aksenov.dds"
					ideology = Conservative
					traits = {
						emerging_Conservative 
						likeable 
						pro_russia
						economist
						career_politician
					}
				}
				create_country_leader = {
					name = "Sergey Aksenov"
					picture = "gfx/leaders/SOV/CRM/sergey_aksenov.dds"
					ideology = Autocracy
					traits = {
						Emerging_Autocracy 
						likeable
						pro_russia
						economist
						career_politician
					}
				}
				if = {
					limit = {
						has_idea = Non_State_Actor
					}
					remove_ideas = Non_State_Actor
				}
			}
		}
	}
	focus = {
		id = CRM_economic_zone
		icon = crimea
		x = 0
		y = 1
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_Pro_russ }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_economic_zone"
			add_ideas = CRM_zone
			set_temp_variable = { treasury_change = -5 }
			669 = {
				add_extra_state_shared_building_slots = 1
			}
			1112 = {
				add_extra_state_shared_building_slots = 1
			}
		}
	}
	focus = {
		id = CRM_ros_banks
		icon = russian_investments
		x = -2
		y = 2
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_economic_zone }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_ros_banks"
			one_office_construction = yes
		}
	}
	focus = {
		id = CRM_promka
		icon = factory_facto
		x = 0
		y = 2
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_economic_zone }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_promka"
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
		}
	}
	focus = {
		id = CRM_airport
		icon = crm_airport
		x = 2
		y = 2
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_economic_zone }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_airport"
			669 = {
				add_building_construction = {
					type = air_base
					level = 2
					instant_build = yes
				}
			}
			set_temp_variable = { treasury_change = -5 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_tourism
		icon = crm_tourizm
		x = 3
		y = 3
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_airport }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_tourism"
			add_ideas = CRM_tourism_idea
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_infra
		icon = Payment_on_roads
		x = 1
		y = 3
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_airport }
		prerequisite = { focus = CRM_promka }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_tourism"
			one_random_infrastructure = yes
		}
	}
	focus = {
		id = CRM_energy
		icon = focus_generic_hydroelectric_energy
		x = -1
		y = 3
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_ros_banks }
		prerequisite = { focus = CRM_promka }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_energy"
			669 = {
				add_building_construction = {
					type = fossil_powerplant
					level = 1
					instant_build = yes
				}
			}
			set_temp_variable = { treasury_change = -3.5 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_ukrep
		icon = city_fort
		x = -2
		y = 4
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_energy }
		prerequisite = { focus = CRM_selhoz }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_ukrep"
			1112 = {
				add_building_construction = {
					type = coastal_bunker
					level = 1
					province = 11649
					instant_build = yes
					}
			}
			1112 = {
				add_building_construction = {
					type = coastal_bunker
					level = 1
					province = 9680
					instant_build = yes
					}
			}
			1112 = {
				add_building_construction = {
					type = coastal_bunker
					level = 1
					province = 705
					instant_build = yes
					}
			}
			669 = {
				add_building_construction = {
					type = coastal_bunker
					level = 1
					province = 3468
					instant_build = yes
					}
			}
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_empire
		icon = monarchism_forced_majority
		x = -2
		y = 5
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_ukrep }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_empire"
			add_political_power = 100
			set_temp_variable = { party_index = 23 }
			set_temp_variable = { party_popularity_increase = 0.035 }
			set_temp_variable = { temp_outlook_increase = 0.035 }
			add_relative_party_popularity = yes
			SOV = {
				set_temp_variable = { party_index = 23 }
				set_temp_variable = { party_popularity_increase = 0.035 }
				set_temp_variable = { temp_outlook_increase = 0.035 }
				add_relative_party_popularity = yes
			}
		}
	}
	focus = {
		id = CRM_cipgro
		icon = economic_it
		x = 2
		y = 4
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_tourism }
		prerequisite = { focus = CRM_infra }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_cipgro"
			add_tech_bonus = {
				name = BLR_Digitalization
				bonus = 0.55
				uses = 2
				category = internet_tech
			}
			set_temp_variable = { treasury_change = -4.60 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_selhoz
		icon = Wheat_Industry
		x = -3
		y = 3
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_ros_banks }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_selhoz"
			add_ideas = CRM_selhoz_idea
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_zavod_ships
		icon = naval_industry
		x = 0
		y = 5
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_cipgro }
		prerequisite = { focus = CRM_ukrep }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_zavod_ships"
			add_ideas = CRM_naval
			set_temp_variable = { treasury_change = -4.5 }
			modify_treasury_effect = yes
			if = {
				limit = { CRM = { is_subject_of = SOV } }
				custom_effect_tooltip = SUB_nat_spirits_added_tt
			}
			hidden_effect = {
				SOV = { add_ideas = CRM_naval }
			}
		}
	}
	focus = {
		id = CRM_rost
		icon = expand_finance_ministry
		x = 0
		y = 6
		cost = 7
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_zavod_ships }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_rost"
			add_political_power = 100
		}
	}
	focus = {
		id = CRM_kazaki
		icon = crm_cossaks
		x = 0
		y = 7
		cost = 5
		available = {
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_rost }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_pmcs"
			add_ideas = CRM_kazachestvo
			set_temp_variable = { treasury_change = -0.2 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = CRM_pmcs
		icon = sov_bars
		x = 0
		y = 8
		cost = 5
		available = {
			has_war = yes
			is_subject_of = SOV
			owns_state = 669
		}
		ai_will_do = { factor = 20 }
		relative_position_id = CRM_Pro_russ
		prerequisite = { focus = CRM_kazaki }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus CRM_pmcs"
			division_template = {
				name = "PMCs Battalion"
				regiments = {
					L_Inf_Bat = { x = 0 y = 0 }
					L_Inf_Bat = { x = 0 y = 1 }
				}
			}
			add_manpower = -2400
			set_temp_variable = { treasury_change = -9 }
			modify_treasury_effect = yes
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 669 }
				create_unit = {
					division = "name = \"PMC Tavrida\" division_template = \"PMCs Battalion\" start_experience_factor = 0.1"
					owner = CRM
				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 669 }
				create_unit = {
					division = "name = \"PMC Konvoy\" division_template = \"PMCs Battalion\" start_experience_factor = 0.1"
					owner = CRM
				}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 669 }
				create_unit = {
					division = "name = \"PMC Livandia\" division_template = \"PMCs Battalion\" start_experience_factor = 0.1"
					owner = CRM
				}
			}
		}
	}
	shared_focus = SUB_russia_politic
}