PAK_heavy_industries_taxila_tank_manufacturer = {
	allowed = { original_tag = PAK }
	icon = GFX_idea_Heavy_Industries_Taxila
	name = PAK_heavy_industries_taxila_tank_manufacturer
	include = generic_tank_equipment_organization
}

PAK_pakistan_aeronautical_complex_aircraft_manufacturer = {
	allowed = { original_tag = PAK }
	icon = GFX_idea_Pakistan_aeronautical
	name = PAK_pakistan_aeronautical_complex_aircraft_manufacturer
	include = generic_air_equipment_organization
}

PAK_integrated_defence_systems_materiel_manufacturer = {
	allowed = { original_tag = PAK }
	icon = GFX_idea_Integraded_Dynamics
	name = PAK_integrated_defence_systems_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

PAK_pakistan_ordnance_factories_materiel_manufacturer = {
	allowed = { original_tag = PAK }
	icon = GFX_idea_Pakistan_Ordnance
	name = PAK_pakistan_ordnance_factories_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

PAK_karachi_shipyard_naval_manufacturer = {
	allowed = { original_tag = PAK }
	icon = GFX_idea_Karachi
	name = PAK_karachi_shipyard_naval_manufacturer
	include = generic_naval_equipment_organization
}

PAK_karachi_shipyard_naval_manufacturer2 = {
	allowed = { original_tag = PAK }
	icon = GFX_idea_Karachi
	name = PAK_karachi_shipyard_naval_manufacturer2
	include = generic_sub_equipment_organization
}