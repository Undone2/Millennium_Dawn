# Issue Name

_Ensure the words ``Focus Tree`` is in the title and the label ``WIP Focus Trees`` is assigned. You should also assign the milestone to the targeted release date. So if you are a 1.10 tree, you should be assigned to the 1.10 milestone. The issue should also have the following:_

Nation: ``Replace me with the nation name``

Branch: ``Replace me with the branch this is related to``

## Purpose

_Replace me with a short description about what the purpose of this issue is. It can be short and concise._

## Importance

_1 to 10 -- 10 being the most important, 1 being the least important. Scaling revolves around the following guidelines: 1-5; Minor Mechanics, 5-8; Country Specific Content, 8-10; Global Mechanic or Changes_

## Description of the Issue

_This section is where you put the "meat" of the issue. Any details, images or otherwise you would like to attach to the issue place below._

/milestone %"Version 1.9 Roadmap"

/assign me

/label ~"WIP Focus Trees"